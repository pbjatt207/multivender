<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Product_varient;
use App\Model\Product;
use App\Model\Shop;
use App\Model\Color;
use App\Model\Size;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Product::latest()->with(['productdata','productvarient'=> function($q) {
            $q->orderBy('sale_price', 'ASC')->with('shop','unit','size','color');
        },'category','brand'])->get();
        foreach($lists as $key => $list) {
            $lists[$key]->sameproduct = $lists[$key]->productvarient;
            $similarproduct = Product::where('category_id', $lists[$key]->category_id)->get();
            $lists[$key]->similarproduct = $similarproduct;
        }
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {

            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $product_id)
    {
        $shop_id = Product_varient::findOrFail($request->productvarient_id);
        $query = Product::with(['productdata','productvarient'=> function($r) use ($shop_id) {
            $r->orderBy('sale_price', 'ASC')->with('shop','unit','size','color')->where('shop_id', $shop_id->shop_id);
        },'category','brand']);
        
        if ($request->size_id != '') {
            $query->with(['productdata','productvarient'=> function($p) use ($request) {
                $p->where('size_id', $request->size_id);
            },'category','brand']);
        }
        
        if ($request->color_id != '') {
            $query->with(['productdata','productvarient'=> function($q) use ($request) {
                $q->where('color_id', $request->color_id);
            },'category','brand']);
        }
        
        if ($request->size_id != '' && $request->color_id != '') {
            $query->with(['productdata','productvarient'=> function($p) use ($request) {
                $p->orderBy('sale_price', 'ASC')->where('size_id', $request->size_id)->where('color_id', $request->color_id);
            },'category','brand']);
        }
        
        $lists = $query->findOrFail($product_id);
        
        $shop_id_is = $shop_id->shop_id;
        $spquery = Product::with(['productdata','productvarient'=> function($r) use ($shop_id_is) {
            $r->orderBy('sale_price', 'ASC')->with('shop','unit','size','color')->whereNotIn('shop_id', [$shop_id_is]);
        },'category','brand']);
        
        if ($request->size_id != '') {
            $spquery->with(['productdata','productvarient'=> function($p) use ($request) {
                $p->where('size_id', $request->size_id);
            },'category','brand']);
        }
        
        if ($request->color_id != '') {
            $spquery->with(['productdata','productvarient'=> function($q) use ($request) {
                $q->where('color_id', $request->color_id);
            },'category','brand']);
        }
        
        if ($request->size_id != '' && $request->color_id != '') {
            $spquery->with(['productdata','productvarient'=> function($p) use ($request) {
                $p->orderBy('sale_price', 'ASC')->where('size_id', $request->size_id)->where('color_id', $request->color_id);
            },'category','brand']);
        }
        $lists1 = $spquery->findOrFail($product_id);
        
        $product = [];
        $product['id'] = $lists->id;
        $product['name'] = $lists->name;
        $product['slug'] = $lists->slug;
        $product['short_name'] = $lists->short_name;
        $product['category'] = $lists->category->name;
        $product['brand'] = $lists->brand->name;
        $product['hsn_code'] = $lists->hsn_code;
        $product['gst_id'] = $lists->gst_id;
        // $product['shop'] = $lists->productvarient[1]->shop->name;
        $product['image'] = $lists->productdata->image;
        $product['description'] = $lists->productdata->description;
        
        $color = [];
        foreach($lists->productvarient as $pv) {
            $c = Color::findOrFail($pv->color_id);
            $color[] = [
                'id' => $pv->color_id,
                'name' => $c->name,
                'image' => $c->image
                ];
        }
        $product['color'] = $color;
        $size = [];
        foreach($lists->productvarient as $pv) {
            $s = Size::findOrFail($pv->size_id);
            $size[] = [
                'id' => $pv->size_id,
                'name' => $s->name
                ];
        }
        $product['size'] = $size;
        
        $varient = [];
        foreach($lists->productvarient as $pv) {
            $varient[] = [
                'id' => $pv->id,
                'product' => $lists->name,
                'shop' => $pv->shop->name,
                'color' => $pv->color->id,
                'size' => $pv->size->id,
                'unit_id' => $pv->unit_id,
                'qty' => $pv->qty,
                'regular_price' => $pv->regular_price,
                'sale_price' => $pv->sale_price,
                'discount' => $pv->discount,
                ];
        }
        $product['productvarient'] = $varient;
        
        $othershopvarient = [];
        foreach($lists1->productvarient as $pv) {
            $othershopvarient[] = [
                'id' => $pv->id,
                'product' => $lists->name,
                'shop' => $pv->shop->name,
                'color' => $pv->color->id,
                'size' => $pv->size->id,
                'unit_id' => $pv->unit_id,
                'qty' => $pv->qty,
                'regular_price' => $pv->regular_price,
                'sale_price' => $pv->sale_price,
                'discount' => $pv->discount,
                ];
        }
        $product['other_shop_productvarient'] = $othershopvarient;
        
        $similarproduct = Product::with(['productdata','productvarient'=> function($q) {
            $q->orderBy('sale_price', 'ASC')->with('shop','unit','size','color');
        },'category','brand'])->where('category_id', $lists->category_id)->get();
        
        $similarpro = [];
        
        foreach($similarproduct as $pv) {
            $spvarient = [];
            foreach($pv->productvarient as $p) {
                $spvarient[] = [
                    'id' => $p->id,
                    'product' => $pv->name,
                    'shop' => $p->shop->name,
                    'color' => $p->color->id,
                    'size' => $p->size->id,
                    'unit_id' => $p->unit_id,
                    'qty' => $p->qty,
                    'regular_price' => $p->regular_price,
                    'sale_price' => $p->sale_price,
                    'discount' => $p->discount,
                    ];
            }
            $similarpro[] = [
            'id' => $pv->id,
            'name' => $pv->name,
            'slug' => $pv->slug,
            'short_name' => $pv->short_name,
            'category' => $pv->category->name,
            'brand' => $pv->brand->name,
            'hsn_code' => $pv->hsn_code,
            'gst_id' => $pv->gst_id,
            'image' => $pv->productdata->image,
            'description' => $pv->productdata->description,
            'productvarients' => $spvarient
            ];
        }
        $product['similarproduct'] = $similarpro;
        
        $re = [
            'status'    => true,
            'data'      => $product
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {
        //
    }
}
