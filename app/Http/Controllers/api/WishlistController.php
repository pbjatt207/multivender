<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Product;
use App\Model\Wishlist;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Wishlist::with(['product'=> function($p) {
            $p->with(['productdata','productvarient'=> function($q) {
                $q->with('shop','unit','size','color');
            },'category','brand']);
        }])->where('user_id', Auth::user()->id)->get();
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {

            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    protected function checkExist($pId, $user_id) {
        if(is_numeric($pId) && is_numeric($user_id)) {
            $info = Wishlist::where('product_id', $pId)->where('user_id', $user_id)->count();
            return $info;
        } else {
            return false;
        }
    }

    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $re = [
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user_id = Auth::user()->id;
            $product = Product::find($request->product_id);
            $pId = $product->id;
            if (!empty($pId)) {
                if($this->checkExist($pId, $user_id)) {
                    // remove
                    $info = Wishlist::where('product_id', $pId)->where('user_id', $user_id)->first();
                    $info->delete();
    
                    $re = [
                        'status'        => true,
                        'message'       => $product->name.' removed to wishlist successfully.'
                    ];
                } else {
                    // add
                    $Arr = [
                        'product_id'    => $pId,
                        'user_id'       => $user_id
                    ];
                    $wishlist = Wishlist::create($Arr);
    
                    $re = [
                        'status'        => true,
                        'message'       => $product->name.' added to wishlist successfully.'
                    ];
                }
                
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Product not found.'
                ];
            }
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function show($wishlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $wishlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy($wishlist)
    {
        //
    }
}
