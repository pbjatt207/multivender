<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists  = Cart::where('user_id', Auth::user()->id)->get();

        if($lists->count() != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cart'        => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $cartJson = $request->cart;
            if (!empty($cartJson)) {
                $cartJso = json_decode($cartJson);
                foreach ($cartJso as $c) {
                    $cart = Cart::where('user_id', Auth::user()->id)->where('product_id', $c->product_id)->where('product_varient_id', $c->product_varient_id)->get();
                    // dd($cart);
                    if ($cart->count() == 0) {
                        $dataArr = [
                            'user_id'           => Auth::user()->id,
                            'product_id'        => $c->product_id,
                            'product_varient_id'=> $c->product_varient_id,
                            'qty'               => $c->qty
                        ];
                        $cart    = Cart::create($dataArr);
                    } else {
                        $cart = Cart::where('user_id', Auth::user()->id)->where('product_id', $c->product_id)->where('product_varient_id', $c->product_varient_id)->first();
                        $cart->qty = $cart->qty + $c->qty;
                        $cart->save();
                    }
                }
                $re = [
                    'status'    => true,
                    'message'   => 'Cart added successfully.',
                ];
            } else {
                $re = [
                    'status'    => true,
                    'message'   => 'No records found.'
                ];
            }
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Cart $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Cart $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        if (!empty($cart->id)) {
            $cart->delete();

            $re = [
                'status'    => true,
                'message'   => 'Product removed successfully.'
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Cart not found.',
            ];
        }
        return response()->json($re);
    }
}
