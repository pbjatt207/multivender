<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Model\Aboutus;
use App\Model\Faq;
use App\Model\Termcondition;
use App\Model\Setting;
use App\Model\Offer;


class PageController extends Controller
{
    public function aboutus()
    {
        $lists = Aboutus::first();
        if (!empty($lists)) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    public function faq()
    {
        $lists = Faq::get();
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    public function termcondition()
    {
        $lists = Termcondition::get();
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    public function setting()
    {
        $lists = Setting::first();
        if (!empty($lists)) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }
    
    public function offer()
    {
        $lists = Offer::get();
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

}
