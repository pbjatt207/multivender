<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Category;
use App\Model\Subcategory;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if($request->show && $request->show == 'all')
        {
            $lists = Category::orderBy('name')->pluck('name', 'slug');
        } else {
            $lists = Category::get();
        }
        
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        $lists = Category::where('category_id', $category)->get();
        if (count($lists) != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        //
    }
}
