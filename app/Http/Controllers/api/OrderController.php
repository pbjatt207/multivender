<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists  = Order::where('user_id', Auth::user()->id)->get();

        if($lists->count() != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($order_id)
    {
        $lists  = Order::with('orderAddress','ordermenu')->where('user_id', Auth::user()->id)->find($order_id);
        
        if($lists != '') {
            $re = [
                'status'    => true,
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
