<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Address;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Address::where('user_id', Auth::user()->id)->get();
        if($lists->count() != 0) {
            $re = [
                'status' => true,
                'message'   => $lists->count()." records found.",
                'data'   => $lists
            ];
        } else {
            $re = [
                'status' => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'house_no'  => 'required',
            'address'   => 'required',
            'longitude' => 'required',
            'latitude'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            
            $dataArr = [
                'user_id'       => Auth::user()->id,  
                'house_no'      => request('house_no'),
                'landmark'      => request('landmark'),
                'address'       => request('address'),
                'longitude'     => request('longitude'),
                'latitude'      => request('latitude')
            ];
            
            $address    = Address::create($dataArr);
            
            $re = [
                'status'    => true,
                'message'   => 'Address added successfully.',
                'data'      =>  $address
            ];
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $address
     * @return \Illuminate\Http\Response
     */
    public function show($address)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $address)
    {
        $validator = Validator::make($request->all(), [
            'house_no'  => 'required',
            'address'   => 'required',
            'longitude' => 'required',
            'latitude'  => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $lists  = Address::find($address);
            $input  = $request->all();
            $lists->fill($input)->save();
            
            $re = [
                'status'    => true,
                'message'   => 'Address updated successfully.',
                'data'      =>  $lists
            ];
        }
        
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy($address)
    {
        $lists = Address::find($address);
        if (!empty($lists)) {
            $lists->delete();
            
            $re = [
                'status'    => true,
                'message'   => 'Address deleted successfully.'
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Address not found.'
            ];
        }        
        return response()->json($re);
    }
}
