<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Model\Coupon;
use App\Model\Couponapply;
use App\Model\Product;
use App\Model\Product_varient;
use App\Model\Cart;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Coupon::get();
        if($lists->count() != 0) {
            $re = [
                'status'    => true,
                'message'   => $lists->count()." records found.",
                'data'      => $lists
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'No records found.'
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'coupon'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $coupon = Coupon::where('name', $request->coupon);
            if ($coupon->count()) {
                $coupon_info = $coupon->first();
                
                $cart = Cart::where('user_id', Auth::user()->id)->get();

                $is_valid    = true;

                $current_date = strtotime(date('d-m-Y'));
-               $valid_from   = strtotime($coupon_info->valid_from);

                $user_limit     = Couponapply::where('user_id', Auth()->user()->id)->where('coupon_id', $coupon_info->id)->count();

                if ($user_limit >= $coupon_info->limit_per_user) {
                    $is_valid = false;
                    $re = [
                        'status'    => false,
                        'message'   => 'Your coupon uses limit complete.'
                    ];
                }

                if ($coupon_info->limit_user == 0) {
                    $is_valid = false;
                    $re = [
                        'status'    => false,
                        'message'   => 'Coupon limit complete.'
                    ];
                }
                if ($valid_from > $current_date) {
                    $is_valid = false;
                    $re = [
                        'status'    => false,
                        'message'   => 'Coupon is not activated yet.'
                    ];
                }
                
                if ($coupon_info->never_end == 0) {
                    $valid_upto   = strtotime($coupon_info->valid_upto);

                    if ($current_date > $valid_upto) {
                        $is_valid = false;
                        $re = [
                            'status'    => false,
                            'message'   => 'Coupon code has been expired.'
                        ];
                    }
                }

                if (!empty($coupon_info->min_cart_amount)) {
                    $cart_total = 0;
                    foreach ($cart as $c) {
                        $productInfo = Product::find($c->product_id);
                        $productVarient = Product_varient::find($c->product_varient_id);
                        $cart_total  += $productVarient->sale_price * $c->qty;
                    }

                    if ($cart_total < $coupon_info->min_cart_amount) {
                        $is_valid = false;
                        $re = [
                            'status'    => false,
                            'message'   => 'Coupon is not valid for cart amount.'
                        ];
                    }
                }
                if ($is_valid) {

                    $re = [
                        'status'    => true,
                        'message'   => 'Coupon code is applied.',
                        'data'      => $coupon_info
                    ];
                }

            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Coupon code is not available.'
                ];
            }
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show($coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy($coupon)
    {
        //
    }
}
