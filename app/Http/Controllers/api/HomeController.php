<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\Model\User;
use App\Model\Slider;
use App\Model\Service;
use App\Model\Offer;
use App\Model\Category;
use App\Model\Product;
use App\Model\Color;
use App\Model\Size;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $slider = Slider::orderBy('id', 'desc')->get();
        $service = Service::orderBy('id', 'asc')->get();
        $offer = Offer::orderBy('id', 'asc')->get();
        $category = Category::orderBy('id', 'asc')->where('category_id',null)->get();
        $subcategory = Category::orderBy('id', 'asc')->where('category_id','<>', null )->get();
        $product = Product::with(['productdata','productvarient'=> function($q) {
            $q->with('shop','unit','size','color');
        },'category','brand'])->paginate(20);

        $re = [
            'slider'    => $slider,
            'service'   => $service,
            'offer'    	=> $offer,
            'category'  => $category,
            'subcategory'  => $subcategory,
            'product'   => $product,
        ];
        return response()->json($re);
    }
    
    public function product(Request $request)
    {
        $category = Category::orderBy('id', 'asc')->where('category_id',null)->get();
        $product = Product::with(['productdata','productvarient'=> function($q) {
            $q->with('shop','unit','size','color');
        },'category','brand'])->get();
        
        $productArr = [];
        
        $productArr[0] = [
        	'title'	=> "Product",
        	'data'	=> $product
        ];
        $productArr[1] = [
        	'title'	=> "Top Shopping Product",
        	'data'	=> $product
        ];
        $productArr[2] = [
        	'title'	=> "Featured Product",
        	'data'	=> $product
        ];
        $productArr[3] = [
        	'title'	=> "Top of the day",
        	'data'	=> $product
        ];
        

        $re = [
            'category'  => $category,
            'product'   => $productArr,
        ];
        return response()->json($re);
    }
    
    public function master(Request $request)
    {
        $size = Size::orderBy('id', 'desc')->get();
        $color = Color::orderBy('id', 'asc')->get();

        $re = [
            'size'    => $size,
            'color'   => $color
        ];
        return response()->json($re);
    }
}
