<?php

namespace App\Http\Controllers\api;

use Hash;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Model\User;

class RegisterController extends Controller
{
    public function checkuser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'    => 'required|numeric|regex:/\d{10}/'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user =  User::where('mobile', $request->mobile)->first();
            if($user != ''){
                if ($user->is_verified == 'Y') {
                    if ($user->email != '' && $user->password != '') {
                        $re = [
                            'status'    => true,
                            'is_verified' => true,
                            'mobile_exists' => true,
                            'details_exists' => true,
                            'message'   => 'Mobile number is already exists and verified. please signin.'
                        ];
                    } else {
                        $re = [
                            'status'    => true,
                            'is_verified' => true,
                            'mobile_exists' => true,
                            'details_exists' => false,
                            'message'   => 'Mobile number is already exists and verifed but details not fulfill. please signup'
                        ];
                    }
                } else {
                    $otp        = rand(100000, 999999);
                    // Send SMS
                    // $msg    = urlencode("Your one time password in ".$setting->site_title." is ".$otp." ");
                    // $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
                    // $sms    = file_get_contents($apiUrl);
                    // $user->otp = $otp;
                    $re = [
                        'status'    => true,
                        'is_verified' => false,
                        'mobile_exists' => true,
                        'message'   => 'Mobile number is not verified. verify otp.'
                    ];
                }
            } else {
                $otp        = rand(100000, 999999);
                // Send SMS
                // $msg    = urlencode("Your one time password in ".$setting->site_title." is ".$otp." ");
                // $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
                // $sms    = file_get_contents($apiUrl);
                // $user->otp = $otp;
                $list           =   new User;
                $list->mobile   = $request->mobile;
                $list->otp      = $otp;
                $list->save();

                $user_details = User::find($list->id);

                $re = [
                    'status'    => false,
                    'mobile_exists' => false,
                    'message'   => 'Mobile number not exits. please verify otp.',
                    'data'       => $user_details
                ];
            }
        }
        return response()->json($re);
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'          => 'required|string|regex:/[A-Za-z ]+/',
            'mobile'        => 'required|string|regex:/\d{10}/',
            'email'         => 'required|email',
            'password'      => 'required|string|same:password',
            'termcondition' => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user  =  User::where('mobile', $request->mobile)->first();
            $dataArr = [
                'name'          => request('name'),
                'email'         => request('email'),
                'password'      => Hash::make(request('password')),
                'device_type'   => request('device_type'),
                'device_id'     => request('device_id'),
                'fcm_id'        => request('fcm_id'),
                'termcondition' => request('termcondition'),
                'role_id'       => '2'
            ];

            $user->fill($dataArr)->save();
            //Generate Otp
            // $otp        = rand(100000, 999999);
            // Send SMS
            // $msg    = urlencode("Your one time password in ".$setting->site_title." is ".$otp." ");
            // $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
            // $sms    = file_get_contents($apiUrl);
            // $user->otp = $otp;
            // $user->save();

            $re = [
                'status'    => true,
                'message'   => 'Success!! Registration successful.',
                'data'      => $user,
            ];
        }
        return response()->json($re);
    }

    public function verifyotp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'    => 'required|numeric|regex:/\d{10}/',
            'otp'       => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user =  User::where('mobile', $request->mobile)->first();
            if($user->otp == request('otp')){
                if ($user->email != '' && $user->password != '') {
                    $user->is_verified   = 'Y';
                    $user->otp           = '';
                    $user->save();
    
                    $re = [
                        'status'    => true,
                        'details_exists'=> true,
                        'message'   => 'Success!! Account verified.',
                        'data'      => $user
                    ];
                } else {
                    $user->is_verified   = 'Y';
                    $user->otp           = '';
                    $user->save();
    
                    $re = [
                        'status'    => true,
                        'details_exists'=> false,
                        'message'   => 'Success!! Account verified but user details not fulfill. please signup.',
                        'data'      => $user
                    ];
                }
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error!! OTP not match.'
                ];
            }
        }
        return response()->json($re);
    }

    public function sendotp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|numeric|regex:/\d{10}/',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user = User::where('mobile', $request->mobile)->first();
            if (!empty($user->id)) {
                //Generate Otp
                $otp        = rand(100000, 999999);
                // Send SMS
                // $msg    = urlencode("Your one time password in ".$setting->site_title." is ".$otp." ");
                // $apiUrl = str_replace(["[message]", "[number]"], [$msg, request('mobile')], $setting->sms_api);
                // $sms    = file_get_contents($apiUrl);
                $user->otp = $otp;
                $user->save();      

                $re = [
                    'status'    => true,
                    'message'   => 'Otp has been sent to '.request('mobile'),
                    'data'      => $otp
                ];
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Mobile number not exits.',
                ];
            }
        }
        return response()->json($re);
    }

}
