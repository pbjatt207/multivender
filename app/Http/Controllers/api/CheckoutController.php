<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Model\Order;
use App\Model\Order_menu;
use App\Model\Product;
use App\Model\Product_varient;
use App\Model\Cart;
use App\Model\Address;
use App\Model\Order_address;
use App\Model\Coupon;
use App\Model\Couponapply;
use App\Model\Wallet;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'billingAddress_id' => 'required',
            'payment_type'      => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $orderArr = [
                'user_id'               => Auth::user()->id,
                'billingAddress_id'     => request('billingAddress_id'),
                'shipping_name'         => request('shipping_name'),
                'shipping_mobile'       => request('shipping_mobile'),
                'shipping_email'        => request('shipping_email'),
                'shippingAddress_id'    => request('shippingAddress_id'),
                'payment_type'          => request('payment_type'),
                'payment_status'        => request('payment_status'),
                'txn_id'                => request('txn_id'),
                'coupon_code'           => request('coupon_code'),
                'notes'                 => request('notes')
            ];

            // Order Products
            $cart = Cart::where('user_id', Auth::user()->id)->get();
            if ($cart->count() != 0) {
                $total = 0;
                foreach ($cart as $cartData) {
                    $productVarient = Product_varient::find($cartData->product_varient_id);
                    $total = $total + $cartData->qty * $productVarient->sale_price;
                }
                if ($request->coupon_code != '') {
                    $coupon_info = Coupon::where('name', $request->coupon_code)->first();
                    if ($coupon_info->id != '') {
                        if ($coupon_info->discount_type == 'percent') {
                            $discount = $total * ( $coupon_info->discount / 100 );
                            $total = $total - $discount;
                        }
                        if ($coupon_info->discount_type == 'flat') {
                            $discount = $coupon_info->discount;
                            $total = $total - $discount;
                        }
                        if ($coupon_info->discount_type == 'caseback') {
                            $discount = $coupon_info->discount;
                            $total = $total;

                            $checkWallet = Wallet::where('user_id', Auth::user()->id)->count();
                            if ($checkWallet == 0) {
                                $wallet = new Wallet;
                                $wallet->user_id = Auth::user()->id; 
                                $wallet->point = $discount;
                                $wallet->save();
                            } else {
                                $wallet = Wallet::where('user_id', Auth::user()->id)->first();
                                $wallet->point = $wallet->point + $coupon_info->discount;
                                $wallet->save();
                            }
                        }
                    }
                }
                $orderArr['discount'] = $discount;
                $orderArr['total_amount'] = $total;
                $order = Order::create($orderArr);

                foreach ($cart as $cartData) {
                    $product = Product::find($cartData->product_id);
                    $productVarient = Product_varient::find($cartData->product_varient_id);
                    if (!empty($product->id)) {
                        $menuArr = [
                            'order_id'          => $order->id,
                            'product_id'        => $product->id,
                            'product_varient_id'=> $productVarient->id,
                            'shop_id'           => $productVarient->shop_id,
                            'qty'               => $cartData->qty,
                            'price'             => $productVarient->sale_price
                        ];
                        $orderMenu = Order_menu::create($menuArr);
                    }
                }
                $billingaddress = Address::find($order->billingAddress_id);

                $address    = new Order_address;
                $address->order_id  = $order->id; 
                $address->billing_house_no  = $billingaddress->house_no; 
                $address->billing_landmark  = $billingaddress->landmark; 
                $address->billing_address   = $billingaddress->address; 
                $address->billing_longitude = $billingaddress->longitude; 
                $address->billing_latitude  = $billingaddress->latitude;
                if ($order->shippingAddress_id != '') {
                    $shippingaddress = Address::find($order->shippingAddress_id);
                    $address->shipping_house_no  = $shippingaddress->house_no; 
                    $address->shipping_landmark  = $shippingaddress->landmark; 
                    $address->shipping_address   = $shippingaddress->address; 
                    $address->shipping_longitude = $shippingaddress->longitude; 
                    $address->shipping_latitude  = $shippingaddress->latitude;
                }
                $address->save();

                $coupon_info->limit_user  = $coupon_info->limit_user-1;
                $coupon_info->save();

                $peruserlimit = new Couponapply;
                $peruserlimit->user_id = Auth::user()->id;
                $peruserlimit->coupon_id = $coupon_info->id;
                $peruserlimit->save();

                $re = [
                    'status'    => true,
                    'message'   => "Order create successfully.",
                    'order'     => $order
                ];
            }else {
                $re = [
                    'status'    => false,
                    'message' => 'Cart empty.'
                ];
            }
       }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Cart  $Cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Cart  $Cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $Cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $Cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }
}
