<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use App\Model\User;

class UserController extends Controller
{

    public function index()
    {
        $lists = User::with('wallet')->find(Auth::user()->id);

        $re = [
            'status'    => true,
            'data'      => $lists
        ];
        
        return response()->json($re);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string|regex:/[A-Za-z ]+/',
            'email'     => 'required|email',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {

            $lists = Auth::user();

            $input = $request->all();
            $details = $lists->fill($input)->save();

            $re = [
                'status'    => true,
                'message'   => 'Success! Information has been updated.',
                'data'      =>  $lists
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changepassowrd(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|string',
            'new_password'     => 'required|string|min:8|same:new_password',
            'confirm_password' => 'required|string|min:8|same:new_password'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {

            $list  = Auth::user();
            $password  =  $list->password;

            if (Hash::check($request->current_password, $password)) {
                $new_password = Hash::make($request->new_password);
                $details = $list->fill(['password' => $new_password])->save();

                $re = [
                    'status'    => true,
                    'message'   => 'Success! Password has been updated.',
                    'data'      =>  $list
                ];
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error! Current password not match.'
                ];
            } 
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
