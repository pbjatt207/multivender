<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Area;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Area::orderBy('id', 'desc')->paginate(10);
        
        // set page and title ------------------
        $page  = 'area.list';
        $title = 'Area list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city = City::orderBy('id', 'desc')->get();
        $cityArr  = ['' => 'Select city'];
        if (!$city->isEmpty()) {
            foreach ($city as $cat) {
                $cityArr[$cat->id] = $cat->name;
            }
        }

        // set page and title ------------------
        $page  = 'area.add';
        $title = 'Add Area';
        $data  = compact('page', 'title', 'cityArr');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'            => 'required|array',
            'record.name'       => 'required|string',
            'record.picode'     => 'required',
            'record.city_id'     => 'required'
        ];
        
        $messages = [
            'record.name'  => 'Please Enter Name.',
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Area;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/area'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url(env('ADMIN_DIR').'/area'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Area $area)
    {
        $editData =  ['record'=>$area->toArray()];
        $request->replace($editData);
        //send to view
        $request->flash();

        $city = City::orderBy('id', 'desc')->get();
        $cityArr  = ['' => 'Select city'];
        if (!$city->isEmpty()) {
            foreach ($city as $cat) {
                $cityArr[$cat->id] = $cat->name;
            }
        }
    
        // set page and title ------------------
        $page = 'area.edit';
        $title = 'Edit Area';
        $data = compact('page', 'title', 'area', 'cityArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        $rules = [
            'record'            => 'required|array',
            'record.name'       => 'required|string',
            'record.picode'     => 'required',
            'record.city_id'     => 'required'
        ];
        $messages = [
            'record.name'  => 'Please Enter Name.'
        ];
        $request->validate( $rules, $messages );
      
        $record           = Area::find($area->id);
        $input            = $request->record;
        $record->fill($input);

        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/area'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        $area->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
