<?php

namespace App\Http\Controllers;

use App\Models\payment_mode;
use Illuminate\Http\Request;

class PaymentModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\payment_mode  $payment_mode
     * @return \Illuminate\Http\Response
     */
    public function show(payment_mode $payment_mode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\payment_mode  $payment_mode
     * @return \Illuminate\Http\Response
     */
    public function edit(payment_mode $payment_mode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\payment_mode  $payment_mode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, payment_mode $payment_mode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\payment_mode  $payment_mode
     * @return \Illuminate\Http\Response
     */
    public function destroy(payment_mode $payment_mode)
    {
        //
    }
}
