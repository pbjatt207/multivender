<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Coupon;
use Image;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Coupon::orderBy('id', 'desc')->paginate(10);

        // set page and title ------------------
        $page  = 'coupon.list';
        $title = 'Coupon List';
        $data  = compact('page', 'title', 'lists');
        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disArr = [
            ''          => 'Select Discount Type',
            "flat"      => "Flat",
            "percent"   => "Percent",
            "caseback"  => "Caseback",
        ];

        // set page and title ------------------
        $page  = 'coupon.add';
        $title = 'Add Coupon';
        $data  = compact('page', 'title', 'disArr');
        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $record         = new Coupon;
        $input          = $request->record;
        $input['slug']  = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/coupon/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        } 

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.coupon.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.coupon.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Coupon $coupon)
    {
        $edit      = Coupon::find($coupon->id);
        $edit_data = ['record' => $edit->toArray()];
        $request->replace($edit_data);
        //send to view
        $request->flash();
        // set page and title ------------------

        $disArr = [
            ''          => 'Select Discount Type',
            "flat"      => "Flat",
            "percent"   => "Percent",
            "caseback"  => "Caseback",
        ];
        
        $page = 'coupon.edit';
        $title = 'Edit Coupon';
        $data = compact('page', 'title', 'coupon', 'disArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $record         = $coupon;
        $input          = $request->record;
        $input['slug']  = $input['slug'] == '' ? Str::slug($input['name'], '-') : $input['slug'];
        $record->fill($input);

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/coupon/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }
        if ($record->save()) {
            return redirect(route('admin.coupon.index'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
