<?php

namespace App\Http\Controllers\admin;

use Image;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Product_data;
use App\Model\Product_varient;
use App\Model\User;
use App\Model\Category;
use App\Model\Brand;
use App\Model\Color;
use App\Model\Size;
use App\Model\Shop;
use App\Model\Operator;
use App\Model\Uom;
use App\Model\Gst_rate;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $guardData = Auth::guard()->user();
        if ( $guardData->role_id == '1' ) {
            $query = Product::with('category','productdata','brand','shop','productvarient');
        }
        if ( $guardData->role_id == '2' ) {
            $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
            $shopData = Shop::find($shopertor->shop_id);

            $query = Product::with('category','productdata','brand','shop','productvarient')->whereHas('productvarient', function($q) use ($shopData) {
                $q->where('shop_id', $shopData->id);
            });
        }

        if($request->search != ""){
            $query->where('name', $request->search);
        }

        $lists = $query->paginate(10);

        // set page and title ------------------
        $page  = 'product.list';
        $title = 'Product list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $category = Category::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }
        if($request->category_id == ''){
            $brands = Brand::orderBy('id', 'desc')->get();
        }else{
            $brands = Brand::orderBy('id', 'desc')->where('category_id', $request->category_id)->get();
        }
        $brandArr  = ['' => 'Select brand'];
        if (!$brands->isEmpty()) {
            foreach ($brands as $cat) {
                $brandArr[$cat->id] = $cat->name;
            }
        }
        $color = Color::orderBy('id', 'desc')->get();
        $colorArr  = ['' => 'Select color'];
        if (!$color->isEmpty()) {
            foreach ($color as $cat) {
                $colorArr[$cat->id] = $cat->name;
            }
        } 
        $size = Size::orderBy('id', 'desc')->get();
        $sizeArr  = ['' => 'Select size'];
        if (!$size->isEmpty()) {
            foreach ($size as $cat) {
                $sizeArr[$cat->id] = $cat->name;
            }
        }
        $shop = Shop::orderBy('id', 'desc')->get();
        $shopArr  = ['' => 'Select shop'];
        if (!$shop->isEmpty()) {
            foreach ($shop as $cat) {
                $shopArr[$cat->id] = $cat->name;
            }
        }  
        $unit = Uom::orderBy('id', 'desc')->get();
        $unitArr  = ['' => 'Select unit'];
        if (!$unit->isEmpty()) {
            foreach ($unit as $cat) {
                $unitArr[$cat->id] = $cat->name;
            }
        }
        $gst = Gst_rate::orderBy('id', 'desc')->get();
        $gstArr  = ['' => 'Select GST % (with all tax inc.)'];
        if (!$gst->isEmpty()) {
            foreach ($gst as $cat) {
                $gstArr[$cat->id] = $cat->gst;
            }
        }
          
        // set page and title ------------------
        $page  = 'product.add';
        $title = 'Add Product';
        $data  = compact('page', 'title', 'brandArr', 'parentArr', 'colorArr', 'sizeArr', 'shopArr', 'unitArr', 'gstArr');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $guardData = Auth::guard()->user();
        $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
        $shopData = Shop::find($shopertor->shop_id);

        $rules = [
            'record.name'       => 'required|string',
            'record.short_name' => 'required|string'
        ];
        $request->validate( $rules );
        $record                     = new Product;
        $input1                     = $request->record;
        $record->fill($input1);
        $record->slug               = Str::slug($record->name, '-');
        $record->save();

        $record1                    = new Product_data;
        $input2                     = $request->record1;

        $record1['product_id']      = $record->id;
        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/product/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $record1['image'] = $name;
        }
        $record1->fill($input2);
        // if ( $guardData->role_id == '2' ) {
        //     $record1->shop_id        = $shopData->id;
        // }
        $record1->save();
        
        if($guardData->role_id == '2'){
            foreach ($request->records as $key => $rec) {
                $record2            = new Product_varient;
                $input = [
                    'product_id'    =>   $record->id,
                    'qty'           =>   $rec['qty'],
                    'unit_id'       =>   $rec['unit_id'],
                    'size_id'       =>   $rec['size_id'],
                    'color_id'      =>   $rec['color_id'],
                    'regular_price' =>   $rec['regular_price'],
                    'sale_price'    =>   $rec['sale_price']
                ];
                $record2->fill($input);
                if ( $guardData->role_id == '2' ) {
                    $record2->shop_id        = $shopData->id;
                }
                $record2->save();
            }
        }

        if($guardData->role_id == '1'){
            return redirect(url(env('ADMIN_DIR').'/product'))->with('success', 'Success! New record has been added.');
        }
        if($guardData->role_id == '2'){
            return redirect(url('shop/product'))->with('success', 'Success! New record has been added.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Product $product)
    {
        $guardData = Auth::guard()->user();
        if($guardData->role_id == '1'){
            $lists = Product_varient::with('color','unit','size')->where('product_id', $product->id)->get();
        }
        if($guardData->role_id == '2'){
            $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
            $shopData = Shop::find($shopertor->shop_id);
            $lists = Product_varient::with('color','unit','size')->where('product_id', $product->id)->where('shop_id', $shopData->id)->get();
        }
        
        // set page and title ------------------
        $page  = 'product.single';
        $title = 'Product Details';
        $data  = compact('page', 'title', 'lists', 'product');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {   
        $edit1 = Product_data::where('product_id', $product->id)->first();
        $edit2 = Product_varient::where('product_id', $product->id)->get();

        $editData =  ['record'=>$product->toArray(),'record1'=>$edit1->toArray(),'records'=>$edit2->toArray()];
        // dd($editData);
        $request->replace($editData);
        //send to view
        $request->flash();

        $category = Category::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }
        if($request->category_id == ''){
            $brands = Brand::orderBy('id', 'desc')->get();
        }else{
            $brands = Brand::orderBy('id', 'desc')->where('category_id', $request->category_id)->get();
        }
        $brandArr  = ['' => 'Select brand'];
        if (!$brands->isEmpty()) {
            foreach ($brands as $cat) {
                $brandArr[$cat->id] = $cat->name;
            }
        }
        $color = Color::orderBy('id', 'desc')->get();
        $colorArr  = ['' => 'Select color'];
        if (!$color->isEmpty()) {
            foreach ($color as $cat) {
                $colorArr[$cat->id] = $cat->name;
            }
        } 
        $size = Size::orderBy('id', 'desc')->get();
        $sizeArr  = ['' => 'Select size'];
        if (!$size->isEmpty()) {
            foreach ($size as $cat) {
                $sizeArr[$cat->id] = $cat->name;
            }
        }
        $shop = Shop::orderBy('id', 'desc')->get();
        $shopArr  = ['' => 'Select shop'];
        if (!$shop->isEmpty()) {
            foreach ($shop as $cat) {
                $shopArr[$cat->id] = $cat->name;
            }
        }  
        $unit = Uom::orderBy('id', 'desc')->get();
        $unitArr  = ['' => 'Select unit'];
        if (!$unit->isEmpty()) {
            foreach ($unit as $cat) {
                $unitArr[$cat->id] = $cat->name;
            }
        }
        $gst = Gst_rate::orderBy('id', 'desc')->get();
        $gstArr  = ['' => 'Select GST % (with all tax inc.)'];
        if (!$gst->isEmpty()) {
            foreach ($gst as $cat) {
                $gstArr[$cat->id] = $cat->gst;
            }
        }
    
        // set page and title ------------------
        $page = 'product.edit';
        $title = 'Edit Product';
        // $data = compact('page', 'title','edit');
        $data  = compact('page', 'title', 'product', 'brandArr', 'parentArr', 'colorArr', 'sizeArr', 'shopArr', 'unitArr', 'gstArr', 'edit2');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $guardData = Auth::guard()->user();
        $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
        $shopData = Shop::find($shopertor->shop_id);

        $rules = [
            
        ];
        $request->validate( $rules );

        if($guardData->role_id == '1') {
            $record                     = $product;
            $input1                     = $request->record;
            $record->fill($input1);
            
            $record->slug               = Str::slug($record->name, '-');
            $record->save();
        }

        if($guardData->role_id == '2') {

            $record1 = Product_data::where('product_id', $product->id)->where('shop_id', $syhopData->id)->first();
            $input2  = $request->record1;

            $record1['product_id']  = $product->id;
            if ($request->hasFile('image')) {
                $file = $request->image;
                $optimizeImage = Image::make($file);
                $optimizePath = public_path().'/images/product/';
                $name = time().$file->getClientOriginalName();
                $optimizeImage->save($optimizePath.$name, 72);
                $record1['image'] = $name;
            }
            $record1->fill($input2);
            $record1['image'] = $name;
            $record1['shop_id'] = $shopData->id;
            $record1->save();

            foreach ($request->records as $key => $rec) {
                if ($rec['id'] != '') {
                    $record2 = Product_varient::where('id', $rec['id'])->first();
                    $input = [
                        'qty'           => $rec['qty'],
                        'unit_id'       => $rec['unit_id'],
                        'size_id'       => $rec['size_id'],
                        'color_id'      => $rec['color_id'],
                        'regular_price' => $rec['regular_price'],
                        'sale_price'    => $rec['sale_price']
                    ];
                    // if ($request->hasFile('varient_image')) {
                    //     $file = $request->varient_image;
                    //     $optimizeImage = Image::make($file);
                    //     $optimizePath = public_path().'/images/product/varients/';
                    //     $name = time().$file->getClientOriginalName();
                    //     $optimizeImage->save($optimizePath.$name, 72);
                    //     $record2['varient_image'] = $name;
                    // }
                    $record2->fill($input);
                    $record2['shop_id'] = $shopData->id;
                    $record2->save();
                } else {
                    $record2            = new Product_varient;
                    $input = [
                        'product_id'    => $product->id,
                        'qty'           => $rec['qty'],
                        'unit_id'       => $rec['unit_id'],
                        'size_id'       => $rec['size_id'],
                        'color_id'      => $rec['color_id'],
                        'regular_price' => $rec['regular_price'],
                        'sale_price'    => $rec['sale_price'],
                    ];
                    // if ($request->hasFile('varient_image')) {
                    //     $file = $request->varient_image;
                    //     $optimizeImage = Image::make($file);
                    //     $optimizePath = public_path().'/images/product/varients/';
                    //     $name = time().$file->getClientOriginalName();
                    //     $optimizeImage->save($optimizePath.$name, 72);
                    //     $record2['varient_image'] = $name;
                    // }
                    $record2->fill($input);
                    $record2['shop_id'] = $shopData->id;
                    $record2->save();
                }
            }
        }

        if($guardData->role_id == '1'){
            return redirect(url(env('ADMIN_DIR').'/product'))->with('success', 'Success! Record has been edided');
        }
        if($guardData->role_id == '2'){
            return redirect(url('shop/product'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function product_search(Request $request)
    {
        // set page and title ------------------
        $page  = 'product.product_search';
        $title = 'Add Product';
        $data  = compact('page', 'title');

        // return data to view
        return view('backend.layout.master', $data);
    }

    public function shop_product_add(Request $request, Product $product)
    {   
        $edit1 = Product_data::where('product_id', $product->id)->first();
        $editData =  ['record'=>$product->toArray(),'record1'=>$edit1->toArray()];
        // dd($editData);
        $request->replace($editData);
        //send to view
        $request->flash();

        $category = Category::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }
        if($request->category_id == ''){
            $brands = Brand::orderBy('id', 'desc')->get();
        }else{
            $brands = Brand::orderBy('id', 'desc')->where('category_id', $request->category_id)->get();
        }
        $brandArr  = ['' => 'Select brand'];
        if (!$brands->isEmpty()) {
            foreach ($brands as $cat) {
                $brandArr[$cat->id] = $cat->name;
            }
        }
        $color = Color::orderBy('id', 'desc')->get();
        $colorArr  = ['' => 'Select color'];
        if (!$color->isEmpty()) {
            foreach ($color as $cat) {
                $colorArr[$cat->id] = $cat->name;
            }
        } 
        $size = Size::orderBy('id', 'desc')->get();
        $sizeArr  = ['' => 'Select size'];
        if (!$size->isEmpty()) {
            foreach ($size as $cat) {
                $sizeArr[$cat->id] = $cat->name;
            }
        }
        $shop = Shop::orderBy('id', 'desc')->get();
        $shopArr  = ['' => 'Select shop'];
        if (!$shop->isEmpty()) {
            foreach ($shop as $cat) {
                $shopArr[$cat->id] = $cat->name;
            }
        }  
        $unit = Uom::orderBy('id', 'desc')->get();
        $unitArr  = ['' => 'Select unit'];
        if (!$unit->isEmpty()) {
            foreach ($unit as $cat) {
                $unitArr[$cat->id] = $cat->name;
            }
        }
        $gst = Gst_rate::orderBy('id', 'desc')->get();
        $gstArr  = ['' => 'Select GST % (with all tax inc.)'];
        if (!$gst->isEmpty()) {
            foreach ($gst as $cat) {
                $gstArr[$cat->id] = $cat->gst;
            }
        }
    
        // set page and title ------------------
        $page = 'product.pro_add';
        $title = 'Edit Product';
        
        $data  = compact('page', 'title', 'product', 'brandArr', 'parentArr', 'colorArr', 'sizeArr', 'shopArr', 'unitArr', 'gstArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product  $product
     * @return \Illuminate\Http\Response
     */
    public function shop_product_save(Request $request, Product $product)
    {
        $guardData = Auth::guard()->user();
        $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
        $shopData = Shop::find($shopertor->shop_id);
        
        $rules = [
            
        ];
        $request->validate( $rules );
        
        // $record1                    = new Product_data;
        // $input2                     = $request->record1;

        // $record1['product_id']      = $product->id;
        // if ($request->hasFile('image')) {
        //     $file = $request->image;
        //     $optimizeImage = Image::make($file);
        //     $optimizePath = public_path().'/images/product/';
        //     $name = time().$file->getClientOriginalName();
        //     $optimizeImage->save($optimizePath.$name, 72);
        //     $record1['image'] = $name;
        // }
        // $record1->fill($input2);
        // $record1['shop_id'] = $shopData->id;
        // $record1->save();
        // dd($record1);

        // $records = Product::where('product_id', $product->id)->get();
        // dd($request->records);
        foreach ($request->records as $key => $rec) {
            $record2            = new Product_varient;
            $input = [
                'product_id'    => $product->id,
                'qty'           => $rec['qty'],
                'unit_id'       => $rec['unit_id'],
                'size_id'       => $rec['size_id'],
                'color_id'      => $rec['color_id'],
                'regular_price' => $rec['regular_price'],
                'sale_price'    => $rec['sale_price'],
            ];
            // if ($request->hasFile('varient_image')) {
            //     $file = $request->varient_image;
            //     $optimizeImage = Image::make($file);
            //     $optimizePath = public_path().'/images/product/varients/';
            //     $name = time().$file->getClientOriginalName();
            //     $optimizeImage->save($optimizePath.$name, 72);
            //     $record2['varient_image'] = $name;
            // }
            $record2->fill($input);
            $record2['shop_id'] = $shopData->id;
            $record2->save();
        }
        return redirect(url('shop/product'))->with('success', 'Success! Record has been edided');
    }

}
