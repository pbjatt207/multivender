<?php

namespace App\Http\Controllers;

use App\Order_menu;
use Illuminate\Http\Request;

class OrderMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order_menu  $order_menu
     * @return \Illuminate\Http\Response
     */
    public function show(Order_menu $order_menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order_menu  $order_menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Order_menu $order_menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order_menu  $order_menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order_menu $order_menu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order_menu  $order_menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order_menu $order_menu)
    {
        //
    }
}
