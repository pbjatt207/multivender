<?php

namespace App\Http\Controllers;

use App\Models\user_bank;
use Illuminate\Http\Request;

class UserBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user_bank  $user_bank
     * @return \Illuminate\Http\Response
     */
    public function show(user_bank $user_bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user_bank  $user_bank
     * @return \Illuminate\Http\Response
     */
    public function edit(user_bank $user_bank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user_bank  $user_bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user_bank $user_bank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user_bank  $user_bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(user_bank $user_bank)
    {
        //
    }
}
