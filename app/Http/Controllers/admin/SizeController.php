<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Size;
use App\Model\Category;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Size::orderBy('id', 'desc')->paginate(10);
        
        // set page and title ------------------
        $page  = 'size.list';
        $title = 'Size list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }

        // set page and title ------------------
        $page  = 'size.add';
        $title = 'Add Size';
        $data  = compact('page', 'title', 'parentArr');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'            => 'required|array',
            'record.name'       => 'required|string',
            'record.short_name' => 'required|string',
        ];
        
        $messages = [
            'record.name'  => 'Please Enter Name.',
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Size;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/size'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url(env('ADMIN_DIR').'/size'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\size  $size
     * @return \Illuminate\Http\Response
     */
    public function show(size $size)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\size  $size
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Size $size)
    {
        $editData =  ['record'=>$size->toArray()];
        $request->replace($editData);
        //send to view
        $request->flash();

        $category = Category::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }
    
        // set page and title ------------------
        $page = 'size.edit';
        $title = 'Edit Size';
        $data = compact('page', 'title', 'size', 'parentArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\size  $size
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Size $size)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'  => 'required|string',
            'record.short_name'  => 'required|string'
        ];
        $messages = [
            'record.name'  => 'Please Enter Name.'
        ];
        $request->validate( $rules, $messages );
      
        $record           = Size::find($size->id);
        $input            = $request->record;
        $record->fill($input);

        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/size'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\size  $size
     * @return \Illuminate\Http\Response
     */
    public function destroy(Size $size)
    {
        $size->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
