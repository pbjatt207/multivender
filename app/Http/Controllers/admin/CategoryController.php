<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Model\Category;
use Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Category::orderBy('id', 'desc')->paginate(10);
    
        // set page and title ------------------
        $page  = 'category.list';
        $title = 'Category list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    public function create()
    {
        $lists = Category::orderBy('id', 'desc')->get();
        // dd($lists);
        $parentArr  = ['' => 'Select Parent category'];
        if (!$lists->isEmpty()) {
            foreach ($lists as $pcat) {
                $parentArr[$pcat->id] = $pcat->name;
            }
        }
      
        // set page and title ------------------
        $page  = 'category.add';
        $title = 'Add Category';
        $data  = compact('page', 'title', 'lists', 'parentArr');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string',
            'image'         => 'required'
        ];
        
        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Category;
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/category/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }

        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-'):$input['slug'];
        $record->fill($input);
        
        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/category'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url(env('ADMIN_DIR').'/category'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Category $category)
    {
        $edit     =  Category::find($category->id);
        // dd($edit);
        $editData =  ['record'=>$edit->toArray()];

        $request->replace($editData);
        //send to view
        $request->flash();

        $lists = Category::orderBy('id', 'desc')->get();
       
        $parentArr  = ['' => 'Select Parent category'];
        if (!$lists->isEmpty()) {
            foreach ($lists as $pcat) {
                $parentArr[$pcat->id] = $pcat->name;
            }
        }        
        
        // set page and title ------------------
        $page = 'category.edit';
        $title = 'Edit Category';
        $data = compact('page', 'title','category', 'parentArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
        $record           = Category::find($id);
        $input            = $request->record;

        if ($request->hasFile('image')) {
            $file = $request->image;
            $optimizeImage = Image::make($file);
            $optimizePath = public_path().'/images/category/';
            $name = time().$file->getClientOriginalName();
            $optimizeImage->save($optimizePath.$name, 72);
            $input['image'] = $name;
        }    
        
        $input['slug']    = $input['slug'] == '' ? Str::slug($input['name'], '-'):$input['slug'];
        $record->fill($input);
        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/category'))->with('success', 'Success! Record has been edided');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        
        Category::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }
}
