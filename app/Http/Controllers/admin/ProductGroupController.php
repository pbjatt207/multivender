<?php

namespace App\Http\Controllers;

use App\Models\product_group;
use Illuminate\Http\Request;

class ProductGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product_group  $product_group
     * @return \Illuminate\Http\Response
     */
    public function show(product_group $product_group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product_group  $product_group
     * @return \Illuminate\Http\Response
     */
    public function edit(product_group $product_group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product_group  $product_group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product_group $product_group)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product_group  $product_group
     * @return \Illuminate\Http\Response
     */
    public function destroy(product_group $product_group)
    {
        //
    }
}
