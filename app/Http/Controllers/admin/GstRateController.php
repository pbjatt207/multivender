<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Gst_rate;

class GstRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Gst_rate::orderBy('id', 'asc')->paginate(10);
        
        // set page and title ------------------
        $page  = 'gst_rate.list';
        $title = 'GST list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // set page and title ------------------
        $page  = 'gst_rate.add';
        $title = 'Add GST';
        $data  = compact('page', 'title');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.gst'    => 'required'
        ];
        
        $messages = [
            'record.gst'  => 'Please Enter GST.',
        ];
        
        $request->validate( $rules, $messages );
        
        $record         = new Gst_rate;
        $record->gst    =   $request->record['gst'];
        $record->igst   =   $request->record['gst'];
        $record->sgst   =   $request->record['gst']/2;
        $record->cgst   =   $request->record['gst']/2;
       
        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/gst_rate'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url(env('ADMIN_DIR').'/gst_rate'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\gst_rate  $gst_rate
     * @return \Illuminate\Http\Response
     */
    public function show(Gst_rate $gst_rate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\gst_rate  $gst_rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Gst_rate $gst_rate)
    {
        $editData =  ['record'=>$gst_rate->toArray()];
        $request->replace($editData);
        //send to view
        $request->flash();
    
        // set page and title ------------------
        $page = 'gst_rate.edit';
        $title = 'Edit GST';
        $data = compact('page', 'title', 'gst_rate');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\gst_rate  $gst_rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gst_rate $gst_rate)
    {
        $rules = [
            'record'      => 'required|array',
            'record.gst'  => 'required'
        ];
        $messages = [
            'record.name'  => 'Please Enter GST.'
        ];
        $request->validate( $rules, $messages );
      
        $record           = Gst_rate::find($gst_rate->id);
        $input            = $request->record;
        $record->fill($input);

        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/gst_rate'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\gst_rate  $gst_rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gst_rate $gst_rate)
    {
        $gst_rate->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
