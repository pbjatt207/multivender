<?php

namespace App\Http\Controllers;

use App\Models\product_varient;
use Illuminate\Http\Request;

class ProductVarientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\product_varient  $product_varient
     * @return \Illuminate\Http\Response
     */
    public function show(product_varient $product_varient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\product_varient  $product_varient
     * @return \Illuminate\Http\Response
     */
    public function edit(product_varient $product_varient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\product_varient  $product_varient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product_varient $product_varient)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\product_varient  $product_varient
     * @return \Illuminate\Http\Response
     */
    public function destroy(product_varient $product_varient)
    {
        //
    }
}
