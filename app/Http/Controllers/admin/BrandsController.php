<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Category;
use App\Model\Brand;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Brand::orderBy('id', 'desc')->paginate(10);
        
        // set page and title ------------------
        $page  = 'brand.list';
        $title = 'Brands list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::orderBy('id', 'desc')->get();
        $categoryArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $categoryArr[$cat->id] = $cat->name;
            }
        }

        // set page and title ------------------
        $page  = 'brand.add';
        $title = 'Add Brand';
        $data  = compact('page', 'title', 'categoryArr');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'            => 'required|array',
            'record.name'       => 'required|string',
            'record.short_name' => 'required|string',
        ];
        
        $messages = [
            'record.name'  => 'Please Enter Name.',
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Brand;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.brand.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.brand.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brands)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $brands)
    {
        $brand  = Brand::find($brands);
        $editData =  ['record'=>$brand->toArray()];
        $request->replace($editData);
        //send to view
        $request->flash();

        $category = Category::orderBy('id', 'desc')->get();
        $categoryArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $categoryArr[$cat->id] = $cat->name;
            }
        }
    
        // set page and title ------------------
        $page = 'brand.edit';
        $title = 'Edit Brands';
        $data = compact('page', 'title', 'brands', 'categoryArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $brands)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'  => 'required|string',
            'record.short_name'  => 'required|string'
        ];
        $messages = [
            'record.name'  => 'Please Enter Name.'
        ];
        $request->validate( $rules, $messages );
      
        $record           = Brand::find($brands);
        $input            = $request->record;
        // dd($input);
        $record->fill($input);

        if ($record->save()) {
            return redirect(route('admin.brand.index'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\brands  $brands
     * @return \Illuminate\Http\Response
     */
    public function destroy($brands)
    {
        $brand = Brand::find($brands);
        $brand->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
