<?php

namespace App\Http\Controllers;

use App\Models\shop_category;
use Illuminate\Http\Request;

class ShopCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\shop_category  $shop_category
     * @return \Illuminate\Http\Response
     */
    public function show(shop_category $shop_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\shop_category  $shop_category
     * @return \Illuminate\Http\Response
     */
    public function edit(shop_category $shop_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\shop_category  $shop_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, shop_category $shop_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\shop_category  $shop_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(shop_category $shop_category)
    {
        //
    }
}
