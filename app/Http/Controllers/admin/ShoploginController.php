<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Model\User;
use App\Model\Operator;

class ShoploginController extends BaseController
{
    public function index()
    {
        return view('shop.inc.login');
    }
    public function checklogin(Request $request)
    {
        $rules = [
            "email"       => "required",
            "password"    => "required"
        ];
        $request->validate($rules);

        $user_data= array(
            'email'     => $request->email,
            'password'  => $request->password
        );
        $user = User::where('email', $request->email)->first();
        if (!empty($user)) {
            if ($user->role_id == 2) {
                $operator = Operator::where('user_id', $user->id)->first();
                if (!empty($operator)) {
                    $is_remembered = !empty($request->remember_me) ? true : false;
                    if (Auth::guard('web')->attempt($user_data, $is_remembered)) {
                        return redirect(route('shop-home'));
                    } else {
                        return redirect()->back()->with('error', 'Credentials not matched.');
                    }
                } else {

                    return redirect()->back()->with('error', 'You are not authorize for shop admin panel.');
                }
            } else {
                return redirect()->back()->with('error', 'You are not authorize for shop admin panel.');
            }
        } else {
            return redirect()->back()->with('error', 'Credentials not matched.');
        }
    }
    public function logout()
    {
        Auth::guard('web')->logout();

        return redirect(route('shoplogin'));
    }

}
