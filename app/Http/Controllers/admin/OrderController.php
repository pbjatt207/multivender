<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\Shop;
use App\Model\Operator;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guardData = Auth::guard()->user();
        if ( $guardData->role_id == '1' ) {
            $lists = Order::orderBy('id', 'desc')->with('user','shop','ordermenu','orderaddress')->paginate(10);
        }
        if ( $guardData->role_id == '2' ) {
            $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
            $shopData = Shop::find($shopertor->shop_id);

            
            $query = Order::orderBy('id', 'desc')->with(['user','shop','ordermenu','orderaddress']);

            $lists = $query->whereHas('ordermenu', function($q) use ($shopData) {
                $q->where('shop_id', $shopData->id);
            })->paginate(10);
        }
      
        // set page and title ------------------
        $page  = 'order.list';
        $title = 'Order list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $guardData = Auth::guard()->user();
        if ( $guardData->role_id == '1' ) {
            $lists = Order::orderBy('id', 'desc')->with(['user','shop','ordermenu' => function($q) {
                $q->with(['shop','product','productvarient' => function($r) {
                    $r->with('size','color','unit');
                }]);
            },'orderaddress'])->find($order->id);
        }
        if ( $guardData->role_id == '2' ) {
            $shopertor = Operator::where('user_id', Auth::guard()->user()->id)->first();
            $shopData = Shop::find($shopertor->shop_id);

            $query = Order::orderBy('id', 'desc')->with(['user','shop','ordermenu','orderaddress']);

            $lists = $query->whereHas('ordermenu', function($q) use ($shopData) {
                $q->where('shop_id', $shopData->id);
            })->find($order->id);
        }
      
        // set page and title ------------------
        $page  = 'order.single';
        $title = 'Order Menus';
        $data  = compact('page', 'title', 'lists');
        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
