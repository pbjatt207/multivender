<?php

namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Model\Shop;
use App\Model\Category;
use App\Model\Product;
use App\Model\User;
use App\Model\Order;

class DashboardController extends BaseController
{
    public function index()
    {
        $shop = Shop::get();
        $category = Category::get();
        $product = Product::get();
        $user = User::whereNotIn('id', [1])->get();
        $order = Order::get();
        
    	$page = 'homepage';
        $title = 'Master Admin Dashboard ';
        $data = compact('page','title','shop','category','product','user','order');

        $guard = Auth::guard()->user()->role_id;
        // dd(Auth::guard()->user()->role);
        if($guard == 1){
		  return view('backend.layout.master', $data);
        }
        if($guard == 2){
          return view('shop.layout.master', $data);
        }
    }

}
