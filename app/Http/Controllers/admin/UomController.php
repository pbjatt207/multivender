<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Category;
use App\Model\Uom;

class UomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Uom::orderBy('id', 'desc')->paginate(10);
        
        // set page and title ------------------
        $page  = 'uom.list';
        $title = 'Uom list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::orderBy('id', 'desc')->get();
        $categoryArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $categoryArr[$cat->id] = $cat->name;
            }
        }
        $parent = Uom::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select parent'];
        if (!$parent->isEmpty()) {
            foreach ($parent as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }

        // set page and title ------------------
        $page  = 'uom.add';
        $title = 'Add Uom';
        $data  = compact('page', 'title', 'categoryArr', 'parentArr');

        // return data to view
        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'            => 'required|array',
            'record.name'       => 'required|string',
            'record.short_name' => 'required|string',
        ];
        
        $messages = [
            'record.name'  => 'Please Enter Name.',
        ];
        
        $request->validate( $rules, $messages );
        
        $record           = new Uom;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/uom'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(url(env('ADMIN_DIR').'/uom'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\uom  $uom
     * @return \Illuminate\Http\Response
     */
    public function show(Uom $uom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\uom  $uom
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Uom $uom)
    {
        $editData =  ['record'=>$uom->toArray()];
        $request->replace($editData);
        //send to view
        $request->flash();

        $category = Category::orderBy('id', 'desc')->get();
        $categoryArr  = ['' => 'Select category'];
        if (!$category->isEmpty()) {
            foreach ($category as $cat) {
                $categoryArr[$cat->id] = $cat->name;
            }
        }
        $parent = Uom::orderBy('id', 'desc')->get();
        $parentArr  = ['' => 'Select parent'];
        if (!$parent->isEmpty()) {
            foreach ($parent as $cat) {
                $parentArr[$cat->id] = $cat->name;
            }
        }
    
        // set page and title ------------------
        $page = 'uom.edit';
        $title = 'Edit Uom';
        $data = compact('page', 'title', 'uom', 'categoryArr', 'parentArr');
        // return data to view

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\uom  $uom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Uom $uom)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'  => 'required|string',
            'record.short_name'  => 'required|string'
        ];
        $messages = [
            'record.name'  => 'Please Enter Name.'
        ];
        $request->validate( $rules, $messages );
      
        $record           = Uom::find($uom->id);
        $input            = $request->record;
        $record->fill($input);

        if ($record->save()) {
            return redirect(url(env('ADMIN_DIR').'/uom'))->with('success', 'Success! Record has been edided');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\uom  $uom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Uom $uom)
    {
        $uom->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
