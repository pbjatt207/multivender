<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
	protected $guarded = [];

	public function product()
    {
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
}
