<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $guarded = [];
    protected $hidden = [
        'category_id'
    ];
}
