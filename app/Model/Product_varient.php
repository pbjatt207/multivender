<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_varient extends Model
{
	protected $guarded = [];

	public function size()
    {
        return $this->hasOne('App\Model\Size', 'id', 'size_id');
    }
    public function color()
    {
        return $this->hasOne('App\Model\Color', 'id', 'color_id');
    }
    public function unit()
    {
        return $this->hasOne('App\Model\Uom', 'id', 'unit_id');
    }
    public function product()
    {
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
}
