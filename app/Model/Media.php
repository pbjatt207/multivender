<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
	use SoftDeletes;
    use HasFactory;
    
    protected $guarded = [];
    protected $casts = [
    	'created_at'	=> 'datetime: d M Y h:i A',
    	'updated_at'	=> 'datetime: d M Y h:i A',
    ];
}
