<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order_menu extends Model
{
    protected $guarded = [];

    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
    public function product()
    {
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
    public function productvarient()
    {
        return $this->hasOne('App\Model\Product_varient', 'id', 'product_varient_id');
    }
}
