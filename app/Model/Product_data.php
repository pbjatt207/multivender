<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product_data extends Model
{
    protected $guarded = [];
    protected $table = 'product_data';

    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
}
