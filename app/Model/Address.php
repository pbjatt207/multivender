<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
	protected $guarded = [];

	public function area()
    {
        return $this->hasOne('App\Model\Area', 'id', 'area_id');
    }
    public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
    }
}
