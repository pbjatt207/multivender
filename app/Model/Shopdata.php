<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Shopdata extends Model
{
	protected $guarded 	= [];
	protected $table 	= "shop_data";

	public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
    }
    public function state()
    {
        return $this->hasOne('App\Model\State', 'id', 'state_id');
    }
    public function area()
    {
        return $this->hasOne('App\Model\Area', 'id', 'area_id');
    }
}
