<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }
    public function shop()
    {
        return $this->hasOne('App\Model\Shop', 'id', 'shop_id');
    }
    public function ordermenu()
    {
        return $this->hasMany('App\Model\Order_menu', 'order_id', 'id');
    }
    public function orderAddress()
    {
        return $this->hasOne('App\Model\Order_address', 'order_id', 'id');
    }
}
