<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('billing_house_no')->nullable();
            $table->string('billing_landmark')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_longitude')->nullable();
            $table->string('billing_latitude')->nullable();
            $table->string('shipping_house_no')->nullable();
            $table->string('shipping_landmark')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_longitude')->nullable();
            $table->string('shipping_latitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_addresses');
    }
}
