<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['middleware' => 'auth:api','namespace' => 'api'], function () {

    Route::resources([
        'cart'      => 'CartController',
        'wishlist'  => 'WishlistController',
        'address'   => 'AddressController',
        'order'     => 'OrderController',
        'coupon'    => 'CouponController',
    ]);

    Route::post('/checkout', 'CheckoutController@store');

    Route::get('/user', 'UserController@index');
    Route::post('/user/edit', 'UserController@edit_profile');
    Route::post('/changepassword', 'UserController@changepassowrd');
    
	Route::post('/home', 'HomeController@index');
	
	Route::post('/product-listing', 'HomeController@product');
	
	Route::get('/master', 'HomeController@master');

    Route::resources([
        'category'  => 'CategoryController',
        'product'  => 'ProductController',
    ]);
    
    Route::get('/aboutus', 'PageController@aboutus');
    Route::get('/faq', 'PageController@faq');
    Route::get('/offer', 'PageController@offer');
	Route::get('/setting', 'PageController@setting');
	
});

Route::group(['namespace' => 'api'], function () {

    Route::post('/checkuser', 'RegisterController@checkuser');
    Route::post('/signup', 'RegisterController@register');
    Route::post('/verifyotp', 'RegisterController@verifyotp');
	Route::post('/sendotp', 'RegisterController@sendotp');

    Route::post('/signin', 'LoginController@login');
    Route::post('/forgotpassword', 'LoginController@forgotpassword');
    Route::get('/termcondition', 'PageController@termcondition');

});
