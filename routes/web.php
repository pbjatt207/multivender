<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear', function () {
    // Artisan::call('route:list');
     $routeCollection = Route::getRoutes();
    //  Route::getRoutes();
    // print_r($routeCollection);
    dd($routeCollection);
    return response()->json($routeCollection);
});
Route::get('/', function () {
    return view('welcome');
});
Route::any('admin', function () {
    return false;
});

