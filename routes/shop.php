<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest', 'namespace' => 'admin'], function () {
    Route::any('/login', 'ShoploginController@index')->name('shoplogin');
    Route::post('main/checklogin', 'ShoploginController@checklogin')->name('shop_checklogin');
});

Route::group(['middleware' => 'auth:web', 'namespace' => 'admin'], function () {
    Route::get('logout', 'ShoploginController@logout');
    Route::get('/', 'ShopController@show')->name('shop-home');
});


Route::group(['middleware' => 'auth:web', 'namespace' => 'admin', 'as' => 'shop.'], function () {
    
    Route::resources([
        'shop'      => 'ShopController',
        'product'   => 'ProductController',
        'team'      => 'OperatorController',
        'order'     => 'OrderController'
    ]);
    
    Route::group(['prefix'=>'profile'], function () {
        Route::get('/', 'UserController@profile');
        Route::post('/', 'UserController@edit_profile');
    });

    Route::post('/order/status', 'AjaxController@order_status')->name('order_status');

    Route::group(['prefix'=>'products'], function () {
        Route::get('/search', 'ProductController@product_search');
        Route::get('/add/{product}', 'ProductController@shop_product_add');
        Route::post('/store/{product}', 'ProductController@shop_product_save');
    });
    
    Route::group(['prefix'=>'ajax'], function () {
        Route::post('search', 'AjaxController@index');
    });

    Route::group(['prefix'=>'setting'], function () {
        Route::get('/', 'SettingController@edit');
        Route::post('/', 'SettingController@update');
    });

});


