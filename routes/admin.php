<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest', 'namespace' => 'admin'], function () {
    Route::any('/login', 'UserController@index')->name('login');
    Route::post('main/checklogin', 'UserController@checklogin')->name('checklogin');
});

Route::group(['middleware' => 'auth:admin', 'namespace' => 'admin'], function () {
    Route::get('logout', 'UserController@logout');
    Route::get('/', 'DashboardController@index')->name('admin-home');
});

Route::group(['middleware' => 'auth:admin', 'namespace' => 'admin', 'as' => 'admin.'], function () {
    
    Route::resources([
        'category'  => 'CategoryController',
        'shop'      => 'ShopController',
        'color'     => 'ColorController',
        'size'      => 'SizeController',
        'uom'       => 'UomController',
        'brand'     => 'BrandsController',
        'role'      => 'RoleController',
        'gst_rate'  => 'GstRateController'
    ]);

    Route::resources([
        'city'      => 'CityController',
        'state'     => 'StateController',
        'country'   => 'CountryController',
        'area'      => 'AreaController',
        'product'   => 'ProductController',
        'order'     => 'OrderController'
    ]);

    Route::resources([
        'about'         => 'AboutusController',
        'slider'        => 'SliderController',
        'faq'           => 'FaqController',
        'offer'         => 'OfferController',
        'service'       => 'ServiceController',
        'termcondition' => 'TermconditionController',
        'coupon'        => 'CouponController'
    ]);

    Route::group(['prefix'=>'user'], function () {
        Route::get('/', 'UserController@list');
    });

    Route::post('/order/status', 'AjaxController@order_status')->name('order_status');

    Route::group(['prefix'=>'profile'], function () {
        Route::get('/', 'UserController@profile');
        Route::post('/', 'UserController@edit_profile');
    });

    Route::group(['prefix'=>'setting'], function () {
        Route::get('/', 'SettingController@edit');
        Route::post('/', 'SettingController@update');
    });

});


