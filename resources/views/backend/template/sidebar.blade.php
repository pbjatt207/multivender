@php
    $guard = Auth::guard()->user()->role_id;
    $guardData = Auth::guard()->user();
    if($guardData->role_id == '2') {
        $shopertor = App\Model\Operator::where('user_id', $guardData->id)->first();
        $shopData = App\Model\Shop::find($shopertor->shop_id);
    }
@endphp
<!-- #Top Bar -->
    <div>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" style="background-color: #353c48;">
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="sidebar-user-panel active">
                        <div class="user-panel">
                            <div class=" image">
                                <!-- <img src="{{ url('assets/images/user/usrbig6.jpg') }}" class="img-circle user-img-circle" alt="User Image" /> -->
                                <img src="{{ url('extraimage/images.jpg') }}" class="img-circle user-img-circle" alt="User Image" />
                            </div>
                        </div>
                        <div class="profile-usertitle">
                            <div class="sidebar-userpic-name"> {{ $guardData->name }} </div>
                            @if($guard=='1')
                            <div class="profile-usertitle-job ">Manager </div>
                            @endif
                            @if($guard=='2')
                            <div class="profile-usertitle-job ">{{ $shopertor->role->name }}</div>
                            @endif
                        </div>
                    </li>
                    <li class="active">
                        @if($guard=='1')
                        <a href="{{ url(env('ADMIN_DIR').'/') }}" class="menu-toggle">
                            <i class="fas fa-tachometer-alt"></i>
                            <span>Home</span>
                        </a>
                        @endif
                    </li>
                    @if($guard=='1')
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <!-- <i class="fas fa-angle-double-down"></i> -->
                            <i class="material-icons">folder</i>
                            <span>Master</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-cookie-bite"></i>
                                    <span>Color</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.color.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.color.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Size</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.size.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.size.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Uom</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.uom.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.uom.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Brands</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.brand.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.brand.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Role</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.role.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.role.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>GST Rate</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.gst_rate.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.gst_rate.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <!-- <i class="fas fa-angle-double-down"></i> -->
                            <i class="material-icons">place</i>
                            <span>Location</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-flag"></i>
                                    <span>Country</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.country.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.country.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-building"></i>
                                    <span>State</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.state.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.state.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-city"></i>
                                    <span>City</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.city.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.city.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-city"></i>
                                    <span>Area</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.area.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.area.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('admin.order.index') }}" onClick="" class="menu-toggle">
                            <i class="material-icons">shop</i>
                            <span>Orders</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-gift"></i>
                            <span>Coupon</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('admin.coupon.create') }}">Add</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.coupon.index') }}">View</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-tasks"></i>
                            <span>Category</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('admin.category.create') }}">Add</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.category.index') }}">View</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="material-icons">shop</i>
                            <span>Shop</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="{{ route('admin.shop.create') }}">Add</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.shop.index') }}">View</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if($guard=='2')
                    <li>
                        <a href="{{ url('shop') }}">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('shop.team.index') }}">
                            <i class="material-icons">people</i>
                            <span>Member</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('shop.order.index') }}">
                            <i class="material-icons">shop</i>
                            <span>Order</span>
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-boxes"></i>
                            <span>Product</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                            @if($guard=='1')
                                <a href="{{ route('admin.product.create') }}">Add</a>
                            @endif
                            @if($guard=='2')
                                <a href="{{ url('/').'/shop/products/search' }}">Add</a>
                            @endif
                            </li>
                            <li>
                            @if($guard=='1')
                                <a href="{{ route('admin.product.index') }}">View</a>
                            @endif
                            @if($guard=='2')
                                <a href="{{ route('shop.product.index') }}">View</a>
                            @endif
                            </li>
                        </ul>
                    </li>
                    @if($guard=='1')
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <!-- <i class="fas fa-angle-double-down"></i> -->
                            <i class="material-icons">folder</i>
                            <span>Pages</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>About Us</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.about.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.about.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Faq</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.faq.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.faq.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Service</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.service.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.service.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Offer</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.offer.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.offer.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <i class="fas fa-mail-bulk"></i>
                                    <span>Term & Condition</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="{{ route('admin.termcondition.create') }}">
                                            <span>Add</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('admin.termcondition.index') }}">
                                            <span>View</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    @endif
                    <!-- <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fab fa-google-play"></i>
                            <span>Apps</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../apps/chat.html">Chat</a>
                            </li>
                            <li>
                                <a href="../apps/task.html">Task Bar</a>
                            </li>
                            <li>
                                <a href="../apps/dragdrop.html">Drag &amp; Drop</a>
                            </li>
                            <li>
                                <a href="../apps/portfolio.html">Portfolio</a>
                            </li>
                            <li>
                                <a href="../apps/contact_list.html">Contact List</a>
                            </li>
                            <li>
                                <a href="../apps/contact_grid.html">Contact Grid</a>
                            </li>
                            <li>
                                <a href="../apps/support.html">Support</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-shopping-cart"></i>
                            <span>E-commerce</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../ecommerce/products.html">Products</a>
                            </li>
                            <li>
                                <a href="../ecommerce/product-detail.html">Product Details</a>
                            </li>
                            <li>
                                <a href="../ecommerce/cart.html">Cart</a>
                            </li>
                            <li>
                                <a href="../ecommerce/product-list.html">Product List</a>
                            </li>
                            <li>
                                <a href="../ecommerce/invoice.html">Invoice</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="../widgets/widget.html">
                            <i class="fas fa-braille"></i>
                            <span>Widgets</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-drafting-compass"></i>
                            <span>User Interface (UI)</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../ui/alerts.html">Alerts</a>
                            </li>
                            <li>
                                <a href="../ui/animations.html">Animations</a>
                            </li>
                            <li>
                                <a href="../ui/badges.html">Badges</a>
                            </li>
                            <li>
                                <a href="../ui/modal.html">Modal</a>
                            </li>
                            <li>
                                <a href="../ui/buttons.html">Buttons</a>
                            </li>
                            <li>
                                <a href="../ui/collapse.html">Collapse</a>
                            </li>
                            <li>
                                <a href="../ui/dialogs.html">Dialogs</a>
                            </li>
                            <li>
                                <a href="../ui/cards.html">Cards</a>
                            </li>
                            <li>
                                <a href="../ui/labels.html">Labels</a>
                            </li>
                            <li>
                                <a href="../ui/list-group.html">List Group</a>
                            </li>
                            <li>
                                <a href="../ui/media-object.html">Media Object</a>
                            </li>
                            <li>
                                <a href="../ui/notifications.html">Notifications</a>
                            </li>
                            <li>
                                <a href="../ui/preloaders.html">Preloaders</a>
                            </li>
                            <li>
                                <a href="../ui/progressbars.html">Progress Bars</a>
                            </li>
                            <li>
                                <a href="../ui/range-sliders.html">Range Sliders</a>
                            </li>

                            <li>
                                <a href="../ui/tabs.html">Tabs</a>
                            </li>
                            <li>
                                <a href="../ui/waves.html">Waves</a>
                            </li>
                            <li>
                                <a href="../ui/typography.html">Typography</a>
                            </li>
                            <li>
                                <a href="../ui/helper-classes.html">Helper Classes</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fab fa-wpforms"></i>
                            <span>Forms</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../forms/basic-form-elements.html">Basic Form</a>
                            </li>
                            <li>
                                <a href="../forms/advanced-form-elements.html">Advanced Form</a>
                            </li>
                            <li>
                                <a href="../forms/form-examples.html">Form Examples</a>
                            </li>
                            <li>
                                <a href="../forms/form-validation.html">Form Validation</a>
                            </li>
                            <li>
                                <a href="../forms/form-wizard.html">Form Wizard</a>
                            </li>
                            <li>
                                <a href="../forms/editors.html">Editors</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-table"></i>
                            <span>Tables</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../tables/normal-tables.html">Normal Tables</a>
                            </li>
                            <li>
                                <a href="../tables/advance-tables.html">Advance Datatables</a>
                            </li>
                            <li>
                                <a href="../tables/export-table.html">Export Table</a>
                            </li>
                            <li>
                                <a href="../tables/child-row-table.html">Child Row Table</a>
                            </li>
                            <li>
                                <a href="../tables/group-table.html">Grouping</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="far fa-images"></i>
                            <span>Medias</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../medias/image-gallery.html">Image Gallery</a>
                            </li>
                            <li>
                                <a href="../medias/carousel.html">Carousel</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-chart-line"></i>
                            <span>Charts</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../charts/echart.html">Echart</a>
                            </li>
                            <li>
                                <a href="../charts/apexchart.html">Apex Chart</a>
                            </li>
                            <li>
                                <a href="../charts/amchart.html">amChart</a>
                            </li>
                            <li>
                                <a href="../charts/flot.html">Flot</a>
                            </li>
                            <li>
                                <a href="../charts/chartjs.html">ChartJS</a>
                            </li>
                            <li>
                                <a href="../charts/sparkline.html">Sparkline</a>
                            </li>
                            <li>
                                <a href="../charts/jquery-knob.html">Jquery Knob</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fab fa-hubspot"></i>
                            <span>Timeline</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../timeline/timeline.html">Timeline 1</a>
                            </li>
                            <li>
                                <a href="../timeline/timeline2.html">Timeline 2</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-paw"></i>
                            <span>Icons</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../icons/material-icons.html">Material Icons</a>
                            </li>
                            <li>
                                <a href="../icons/font-awesome.html">Font Awesome</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-id-card"></i>
                            <span>Authentication</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../examples/login-register.html">Login &amp; Register</a>
                            </li>
                            <li>
                                <a href="../examples/login.html">Login</a>
                            </li>
                            <li>
                                <a href="../examples/login2.html">Login 2</a>
                            </li>
                            <li>
                                <a href="../examples/register.html">Register</a>
                            </li>
                            <li>
                                <a href="../examples/register2.html">Register 2</a>
                            </li>
                            <li>
                                <a href="../examples/forgot-password.html">Forgot Password</a>
                            </li>
                            <li>
                                <a href="../examples/locked.html">Locked</a>
                            </li>
                            <li>
                                <a href="../examples/404.html">404 - Not Found</a>
                            </li>
                            <li>
                                <a href="../examples/500.html">500 - Server Error</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="far fa-file-alt"></i>
                            <span>Extra Pages</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../examples/profile.html">Profile</a>
                            </li>
                            <li>
                                <a href="../examples/pricing.html">Pricing</a>
                            </li>
                            <li>
                                <a href="../examples/faqs.html">Faqs</a>
                            </li>
                            <li>
                                <a href="../examples/blank.html">Blank Page</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-globe-americas"></i>
                            <span>Maps</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="../maps/google.html">Google Map</a>
                            </li>
                            <li>
                                <a href="../maps/jqvmap.html">Vector Map</a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- <li>
                        <a href="#" onClick="return false;" class="menu-toggle">
                            <i class="fas fa-angle-double-down"></i>
                            <span>Multi Level Menu</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#" onClick="return false;">
                                    <span>Menu Item</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" onClick="return false;">
                                    <span>Menu Item - 2</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" onClick="return false;" class="menu-toggle">
                                    <span>Level - 2</span>
                                </a>
                                <ul class="ml-menu">
                                    <li>
                                        <a href="#" onClick="return false;">
                                            <span>Menu Item</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onClick="return false;" class="menu-toggle">
                                            <span>Level - 3</span>
                                        </a>
                                        <ul class="ml-menu">
                                            <li>
                                                <a href="#" onClick="return false;">
                                                    <span>Level - 4</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation">
                    <a href="#skins" data-toggle="tab" class="active">SKINS</a>
                </li>
                <li role="presentation">
                    <a href="#settings" data-toggle="tab">SETTINGS</a>
                </li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane in active in active stretchLeft" id="skins">
                    <div class="demo-skin">
                        <div class="rightSetting">
                            <p>GENERAL SETTINGS</p>
                            <ul class="setting-list list-unstyled m-t-20">
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Save
                                                History
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Show
                                                Status
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Auto
                                                Submit Issue
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-check">
                                        <div class="form-check m-l-10">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="" checked> Show
                                                Status To All
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="rightSetting">
                            <p>SIDEBAR MENU COLORS</p>
                            <button type="button"
                                class="btn btn-sidebar-light btn-border-radius p-l-20 p-r-20">Light</button>
                            <button type="button"
                                class="btn btn-sidebar-dark btn-default btn-border-radius p-l-20 p-r-20">Dark</button>
                        </div>
                        <div class="rightSetting">
                            <p>THEME COLORS</p>
                            <button type="button"
                                class="btn btn-theme-light btn-border-radius p-l-20 p-r-20">Light</button>
                            <button type="button"
                                class="btn btn-theme-dark btn-default btn-border-radius p-l-20 p-r-20">Dark</button>
                        </div>
                        <div class="rightSetting">
                            <p>SKINS</p>
                            <ul class="demo-choose-skin choose-theme list-unstyled">
                                <li data-theme="black" class="actived">
                                    <div class="black-theme"></div>
                                </li>
                                <li data-theme="white">
                                    <div class="white-theme white-theme-border"></div>
                                </li>
                                <li data-theme="purple">
                                    <div class="purple-theme"></div>
                                </li>
                                <li data-theme="blue">
                                    <div class="blue-theme"></div>
                                </li>
                                <li data-theme="cyan">
                                    <div class="cyan-theme"></div>
                                </li>
                                <li data-theme="green">
                                    <div class="green-theme"></div>
                                </li>
                                <li data-theme="orange">
                                    <div class="orange-theme"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="rightSetting">
                            <p>Disk Space</p>
                            <div class="sidebar-progress">
                                <div class="progress m-t-20">
                                    <div class="progress-bar l-bg-cyan shadow-style width-per-45" role="progressbar"
                                        aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="progress-description">
                                    <small>26% remaining</small>
                                </span>
                            </div>
                        </div>
                        <div class="rightSetting">
                            <p>Server Load</p>
                            <div class="sidebar-progress">
                                <div class="progress m-t-20">
                                    <div class="progress-bar l-bg-orange shadow-style width-per-63" role="progressbar"
                                        aria-valuenow="63" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="progress-description">
                                    <small>Highly Loaded</small>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane stretchRight" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever switch-col-green"></span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox">
                                        <span class="lever switch-col-blue"></span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever switch-col-purple"></span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever switch-col-cyan"></span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox" checked>
                                        <span class="lever switch-col-red"></span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label>
                                        <input type="checkbox">
                                        <span class="lever switch-col-lime"></span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </div>