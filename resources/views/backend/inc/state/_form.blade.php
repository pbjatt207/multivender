@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
@endif

@if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
    <div class="col-lg-6">
        {{Form::text('record[name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter state name','required'=>'required'])}}
        {{Form::label('record[name]', 'Enter state name'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[short_name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter state short name','required'=>'required'])}}
        {{Form::label('record[short_name]', 'Enter state short name'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::select('record[country_id]', $countryArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record[country_id]', 'Select country'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[code]', '', ['class' => 'squareInput', 'placeholder'=>'Enter state code','required'=>'required'])}}
        {{Form::label('record[code]', 'Enter state code'), ['class' => 'active']}}
    </div>
</div>
