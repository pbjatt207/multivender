@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
@endif

@if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
    <div class="col-lg-6">
        {{Form::text('record[name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter area name','required'=>'required'])}}
        {{Form::label('record[name]', 'Enter area name'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[pincode]', '', ['class' => 'squareInput', 'placeholder'=>'Enter pincode','required'=>'required'])}}
        {{Form::label('record[pincode]', 'Enter pincode'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::select('record[city_id]', $cityArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record[city_id]', 'Select city'), ['class' => 'active']}}
    </div>
</div>
