@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
@endif

@if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
    <div class="col-lg-6">
        {{Form::text('record[name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter coupon name','required'=>'required'])}}
        {{Form::label('record[name]', 'Enter coupon name'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[tagline]', '', ['class' => 'squareInput', 'placeholder'=>'Enter tagline','required'=>'required'])}}
        {{Form::label('record[tagline]', 'Enter tagline'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6 d-none">
        {{ Form::text('record[slug]','', ['class'=>'squareInput', 'placeholder'=>'Enter slug']) }}
        {{ Form::label('slug', 'Enter slug'), ['class' => 'active'] }}
    </div>
    <div class="col-lg-6">
        {{Form::select('record[discount_type]', $disArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record[discount_type]', 'Select discount type'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[discount]', '', ['class' => 'squareInput', 'placeholder'=>'Enter discount','required'=>'required'])}}
        {{Form::label('record[discount]', 'Enter discount'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[valid_from]', '', ['class' => 'squareInput', 'placeholder'=>'Enter valid from','required'=>'required'])}}
        {{Form::label('record[valid_from]', 'Enter valid from'), ['class' => 'active']}}
    </div>
    <span style="position: absolute; top: 105px; left: 52%; display: flex;"><input type="checkbox" name="record[never_end]" value="1" id="show" onclick="$('.valid_upto').slideToggle(function(){$('#show').html($('.valid_upto').is(':visible'));});" style="width: 15px;"><p style="padding-top: 12px; font-size: 12px; padding-left: 5px;"> Never End</p></span>
    <div class="col-lg-6">
      <div class="valid_upto">
        {{Form::text('record[valid_upto]', '', ['class' => 'squareInput ', 'placeholder'=>'Enter valid upto'])}}
        {{Form::label('record[valid_upto]', 'Enter valid upto'), ['class' => 'active']}}
      </div>
    </div>
    <div class="col-lg-6">
        {{Form::text('record[total_coupon]', '', ['class' => 'squareInput', 'placeholder'=>'Enter total coupon','required'=>'required'])}}
        {{Form::label('record[total_coupon]', 'Enter total coupon'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[min_cart_amount]', '', ['class' => 'squareInput', 'placeholder'=>'Enter min cart amount','required'=>'required'])}}
        {{Form::label('record[min_cart_amount]', 'Enter min cart amount'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[limit_user]', '', ['class' => 'squareInput', 'placeholder'=>'Enter limit user','required'=>'required'])}}
        {{Form::label('record[limit_user]', 'Enter limit user'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::text('record[limit_per_user]', '', ['class' => 'squareInput', 'placeholder'=>'Enter limit per user','required'=>'required'])}}
        {{Form::label('record[limit_per_user]', 'Enter limit per user'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::file('image',['class'=>'form-control squareInput'])}}
        {{Form::label('image', 'Choose image'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6"></div>
    <div class="col-lg-6">
        {{ Form::textarea('record[description]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter description']) }}
        {{ Form::label('record[description]', 'Enter description'), ['class' => 'active'] }}
    </div>
    <div class="col-lg-6">
        {{ Form::textarea('record[termcondition]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter term & condition']) }}
        {{ Form::label('record[termcondition]', 'Enter term & condition'), ['class' => 'active'] }}
    </div>
</div>
<!-- <hr>
<div class="row">
   <div class="col-lg-12">
        {{Form::text('record[seo_title]', '', ['class' => 'squareInput', 'placeholder'=>'Enter seo title','required'=>'required'])}}
        {{Form::label('seo_title', 'Enter seo title'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{ Form::textarea('record[seo_keywords]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter seo keyword', 'row' => '4']) }}
        {{ Form::label('seo_keywords', 'Enter seo keyword'), ['class' => 'active'] }}
    </div>
    
    <div class="col-lg-6">
        {{ Form::textarea('record[seo_description]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter seo description', 'row' => '4']) }}
        {{ Form::label('seo_description', 'Enter seo description'), ['class' => 'active'] }}
    </div>
</div> -->
