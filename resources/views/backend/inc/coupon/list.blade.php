<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Coupon List</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{ route('admin-home') }}">
                                    <i class="fas fa-home"></i> Home</a>
                            </li>
                            <li class="breadcrumb-item active">Coupon List</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="pt-2"><b>Coupon List</b></h2>
                            <h2 class="header-dropdown m-r--5"><a href="{{ route('admin.coupon.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add Coupon</a></h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example contact_list">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Tagline</th>
                                            <th>Image</th>
                                            <th>Discount Type</th>
                                            <th>Discount</th>
                                            <th>Valid From</th>
                                            <th>Valid Upto</th>
                                            <th>Total Coupon</th>
                                            <th>Min Cart Amount</th>
                                            <th>Limit User</th>
                                            <th>Limit Per User</th>
                                            <th>Description</th>
                                            <th>Term & Condition</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $sn = $lists->firstItem();
                                        @endphp
                                        @foreach($lists as $list)
                                        <tr>
                                            <td>{{ $sn++ }}</td>
                                            <td>{{ $list->name }}</td>
                                            <td>{{ $list->tagline }}</td>
                                            <td class="table-img" style="width: 10%;">
                                                <img src="{{ url('/').'/images/coupon/'.$list->image }}" alt="">
                                            </td>
                                            <td>{{ $list->discount_type }}</td>
                                            <td>{{ $list->discount }}</td>
                                            <td>{{ $list->valid_from }}</td>
                                            <td>{{ $list->valid_upto }}</td>
                                            <td>{{ $list->total_coupon }}</td>
                                            <td>{{ $list->min_cart_amount }}</td>
                                            <td>{{ $list->limit_user }}</td>
                                            <td>{{ $list->limit_per_user }}</td>
                                            <td>{{ $list->description }}</td>
                                            <td>{{ $list->termcondition }}</td>
                                            <td>
                                                <button class="btn tblActnBtn">
                                                    <a href="{{ route('admin.coupon.edit',$list->id) }}" style="color: black;"><i class="material-icons">mode_edit</i></a>
                                                </button>
                                                {{ Form::open(array('url' => route('admin.coupon.destroy',$list->id), 'class' => 'btn tblActnBtn')) }}
                                                    {{ Form::hidden('_method', 'DELETE') }}
                                                    <button class="btn tblActnBtn">
                                                    <a style="color: black;"><i class="material-icons">delete</i></a>
                                                </button>
                                                {{ Form::close() }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
