@php
    $guardData = Auth::guard()->user();
@endphp
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Shop Edit</h4>
                        </li>
                        @if($guardData->role_id == '1')
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="{{ route('admin.shop.index') }}">Shop</a>
                        </li>
                        @endif
                        @if($guardData->role_id == '2')
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('shop-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="{{ route('shop.shop.index') }}">Shop</a>
                        </li>
                        @endif
                        <li class="breadcrumb-item active">Shop Edit</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="header">
                            <h2 class="pt-2"><b>Shop Edit</b></h2>
                            @if($guardData->role_id == '1')
                            <h2 class="header-dropdown m-r--5" style="top:10px;"><a href="{{ route('admin.shop.index') }}" class="btn btn-primary" style="padding-top: 8px;">View Shop</a></h2>
                            @endif
                            @if($guardData->role_id == '2')
                            <h2 class="header-dropdown m-r--5" style="top:10px;"><a href="{{ url('shop') }}" class="btn btn-primary" style="padding-top: 8px;">View Shop</a></h2>
                            @endif
                        </div>
                        <hr>
                        <div class="formCard">
                            <div class="wrapper">
                                @if($guardData->role_id == '1')
                                {{ Form::open(['url' => route('admin.shop.update',$shop->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                                @endif
                                @if($guardData->role_id == '2')
                                {{ Form::open(['url' => route('shop.shop.update',$shop->id), 'method'=>'PUT','files' => true, 'class' => 'user']) }}
                                @endif
                                @include('backend.inc.shop._form')
                                    <div class="text-right">
                                        <input type="submit"class="btn btn-primary" name="edit_shop" value="Update"/>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






