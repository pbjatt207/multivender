@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
@endif

@if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
   <div class="col-lg-6">
        {{Form::text('record[name]', '', ['class' => 'squareInput', 'placeholder'=>'Enter shop name','required'=>'required'])}}
        {{Form::label('name', 'Enter shop name'), ['class' => 'active']}}
    </div>
    @if($guardData->role_id == '1')
    <div class="col-lg-6">
        {{Form::select('record[user_id]', $userArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record[user_id]', 'Select User'), ['class' => 'active']}}
    </div>
    @endif
    <div class="col-lg-6">
        {{Form::select('record[category_id]', $parentArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record[category_id]', 'Select category'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{ Form::text('record[pan]','', ['class'=>'squareInput', 'placeholder'=>'Enter pan number']) }}
        {{ Form::label('pan', 'Enter pan number'), ['class' => 'active'] }}
    </div>
    <div class="col-lg-6">
        {{ Form::text('record[weburl]','', ['class'=>'squareInput', 'placeholder'=>'Enter web url']) }}
        {{ Form::label('weburl', 'Enter web url'), ['class' => 'active'] }}
    </div>
    <div class="col-lg-6">
        {{Form::file('image',['class'=>'form-control squareInput'])}}
        {{Form::label('image', 'Choose image'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::file('logo',['class'=>'form-control squareInput'])}}
        {{Form::label('logo', 'Choose logo'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::file('fav_icon',['class'=>'form-control squareInput'])}}
        {{Form::label('fav_icon', 'Choose favicon'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::select('record_data[state_id]', $stateArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record_data[state_id]', 'Select state'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::select('record_data[city_id]', $cityArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record_data[city_id]', 'Select city'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{Form::select('record_data[area_id]', $areaArr,'0', ['class' => 'squareInput des-select form-control'])}}
        {{Form::label('record_data[area_id]', 'Select area'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{ Form::text('record_data[address]','', ['class'=>'squareInput', 'placeholder'=>'Enter address']) }}
        {{ Form::label('weburl', 'Enter address'), ['class' => 'active'] }}
    </div>
    <div class="col-lg-6">
        {{ Form::textarea('record_data[description]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter description']) }}
        {{ Form::label('description', 'Enter description'), ['class' => 'active'] }}
    </div>
</div>
<hr>
<div class="row">
   <div class="col-lg-12">
        {{Form::text('record[seo_title]', '', ['class' => 'squareInput', 'placeholder'=>'Enter seo title','required'=>'required'])}}
        {{Form::label('seo_title', 'Enter seo title'), ['class' => 'active']}}
    </div>
    <div class="col-lg-6">
        {{ Form::textarea('record[seo_keywords]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter seo keyword', 'row' => '4']) }}
        {{ Form::label('seo_keywords', 'Enter seo keyword'), ['class' => 'active'] }}
    </div>
    
    <div class="col-lg-6">
        {{ Form::textarea('record[seo_description]','', ['class'=>'squareInput des-textarea', 'placeholder'=>'Enter seo description', 'row' => '4']) }}
        {{ Form::label('seo_description', 'Enter seo description'), ['class' => 'active'] }}
    </div>
</div>
