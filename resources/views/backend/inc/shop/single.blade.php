@php
    $guardData = Auth::guard()->user();
@endphp
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @if($guardData->role_id == 1)
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Shop Details</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{ route('admin-home') }}">
                                    <i class="fas fa-home"></i> Home</a>
                            </li>
                            <li class="breadcrumb-item bcrumb-2">
                                <a href="{{ url('admin/shop') }}" onClick="return false;">Shop</a>
                            </li>
                            <li class="breadcrumb-item active">Shop Details</li>
                        </ul>
                    @endif
                    @if($guardData->role_id == '2')
                        <ul class="breadcrumb breadcrumb-style ">
                            <li class="breadcrumb-item">
                                <h4 class="page-title">Dashboard</h4>
                            </li>
                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{ route('shop-home') }}">
                                    <i class="fas fa-home"></i> Home</a>
                            </li>
                            <li class="breadcrumb-item active">Shop Dashboard</li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>
        <!-- Your content goes here  -->
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="m-b-20">
                        <div class="contact-grid">
                            <div class="profile-header bg-dark">
                                <div class="user-name" style="padding-top: 25px;">{{ $lists->name }}</div>
                                <!-- <div class="name-center">Software Engineer</div> -->
                            </div>
                            <img src="{{ url('/').'/images/shop/'.$lists->shopdata->logo }}" class="user-img" alt="">
                            <p>
                                {{ $lists->shopdata->address }}, {{ $lists->shopdata->city->name }}, {{ $lists->shopdata->state->name }}
                                <br />{{ $lists->shopdata->state->country->name }}
                            </p>
                            <div>
                                <span class="phone">
                                    <i class="material-icons">phone</i>{{ $lists->mobile }}</span>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <h5>{{ count($product) }}</h5>
                                    <small>Products</small>
                                </div>
                                <div class="col-4">
                                    <h5>{{ count($order) }}</h5>
                                    <small>Orders</small>
                                </div>
                                <div class="col-4">
                                    <h5>565</h5>
                                    <small>Users</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12">
                <!-- <div class="card">
                    <div class="profile-tab-box">
                        <div class="p-l-20">
                            <ul class="nav ">
                                <li class="nav-item tab-all">
                                    <a class="nav-link active show" href="#project" data-toggle="tab">About Me</a>
                                </li>
                                <li class="nav-item tab-all p-l-20">
                                    <a class="nav-link" href="#usersettings" data-toggle="tab">Settings</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="project" aria-expanded="true">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="card project_widget">
                                    <div class="header">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h2>About</h2>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                                                @if($guardData->role_id == 1)
                                                <h2><a href="{{ route('admin.shop.edit',$lists->id) }}" style="color: #5b626b;"><i class="material-icons" style="font-size: 18px;">mode_edit</i> Edit</a></h2>
                                                @endif
                                                @if($guardData->role_id == 2)
                                                <h2><a href="{{ route('shop.shop.edit',$lists->id) }}" style="color: #5b626b;"><i class="material-icons" style="font-size: 18px;">mode_edit</i> Edit</a></h2>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="body" style="min-height: 320px;">
                                        <div class="row">
                                            <div class="col-md-3 col-6 b-r">
                                                <strong>Mobile</strong>
                                                <br>
                                                <p class="text-muted">{{ $lists->mobile }}</p>
                                            </div>
                                            <div class="col-md-3 col-6 b-r">
                                                <strong>Email</strong>
                                                <br>
                                                <p class="text-muted">{{ $lists->email }}</p>
                                            </div>
                                            <div class="col-md-3 col-6 b-r">
                                                <strong>Web Url</strong>
                                                <br>
                                                <p class="text-muted">{{ $lists->weburl }}</p>
                                            </div>
                                            <div class="col-md-3 col-6">
                                                <strong>Location</strong>
                                                <br>
                                                <p class="text-muted">{{ $lists->shopdata->city->name }}, {{ $lists->shopdata->state->name }}, {{ $lists->shopdata->state->country->name }}</p>
                                            </div>
                                            <div class="col-md-3 col-6 b-r">
                                                <strong>Pan Number</strong>
                                                <br>
                                                <p class="text-muted">{{ $lists->pan }}</p>
                                            </div>
                                            <div class="col-md-3 col-6 b-r">
                                                <strong>GST Number</strong>
                                                <br>
                                                <p class="text-muted">{{ $lists->gstin }}</p>
                                            </div>
                                        </div>
                                        <p class="m-t-30">
                                            <strong>Description</strong><br>
                                            {{ $lists->shopdata->description }}
                                        </p>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($guardData->role_id == '2')
        <div class="row clearfix">
            <div class="col-lg-3 col-sm-6"><a href="{{ route('shop.product.index') }}">
                <div style="min-height:auto;" class="support-box text-center bg-green">
                    <div class="icon m-b-10">
                        <!-- <div class="chart chart-bar"></div> -->
                    </div>
                    <div class="text m-b-10">Total Products</div>
                    <h3 class="m-b-0">{{ count($product) }}
                        <!-- <i class="material-icons">trending_up</i> -->
                    </h3>
                    <!-- <small class="displayblock">21% Higher Than Average </small> -->
                </div></a>
            </div>
            <div class="col-lg-3 col-sm-6"><a href="{{ route('shop.order.index') }}">
                <div style="min-height:auto;" class="support-box text-center bg-orange">
                    <div class="icon m-b-10">
                        <!-- <span class="chart chart-line"></span> -->
                    </div>
                    <div class="text m-b-10">Orders Received</div>
                    <h3 class="m-b-0">{{ count($order) }}
                        <!-- <i class="material-icons">trending_up</i> -->
                    </h3>
                    <!-- <small class="displayblock">13% Highr Than Average </small> -->
                </div></a>
            </div>
            <div class="col-lg-3 col-sm-6"><a href="{{ route('shop.order.index') }}">
                <div style="min-height:auto;" class="support-box text-center bg-cyan">
                    <div class="icon m-b-10">
                        <!-- <div class="chart chart-pie"></div> -->
                    </div>
                    <div class="text m-b-10">Orders Delivered</div>
                    <h3 class="m-b-0">{{ count($deliveredOrder) }}
                        <!-- <i class="material-icons">trending_down</i> -->
                    </h3>
                    <!-- <small class="displayblock">34% Lower Than Average </small> -->
                </div></a>
            </div>
            <div class="col-lg-3 col-sm-6"><a href="{{ route('shop.order.index') }}">
                <div style="min-height:auto;" class="support-box text-center bg-purple">
                    <div class="icon m-b-10">
                        <!-- <div class="chart chart-bar"></div> -->
                    </div>
                    <div class="text m-b-10">Total Order</div>
                    <h3 class="m-b-0">{{ count($order) }}
                        <!-- <i class="material-icons">trending_down</i> -->
                    </h3>
                    <!-- <small class="displayblock">06% Lower Than Average </small> -->
                </div></a>
            </div>
        </div>
        @endif
    </div>
</section>